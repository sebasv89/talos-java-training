import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class MyFirstClass {

	@Test
	public void helloWorld() {

		// Setup
		WebDriver webDriver = new FirefoxDriver();

		// Exercise
		webDriver.get("http://localhost:8080/safebuy/registrarComprador.html");

		// Verify
		String title = webDriver.getTitle();
		Assert.assertEquals("Registrar Comprador", title);

		// Tear down
		webDriver.close();

	}
}
