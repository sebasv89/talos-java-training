package com.talosdigital.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class RegisterBuyerPage {

	private final WebDriver webDriver;

	public RegisterBuyerPage() {
		webDriver = new FirefoxDriver();
		webDriver.get("http://localhost:8080/safebuy/registrarComprador.html");
	}

	public String getTitle() {
		return webDriver.getTitle();
	}

	public void close() {
		webDriver.close();
	}

	public void submit() {
		WebElement button = webDriver.findElement(By.id("sendButton"));
		button.submit();
	}

	public String getErrorMessage() {
		return webDriver.findElement(By.className("error")).getText();
	}

	public String getSuccessMessage() {
		return webDriver.findElement(By.className("success")).getText();
	}

	public void fillForm(String userName, String name, String lastName,
			String birthday, String email) {
		webDriver.findElement(By.xpath("//input[@name= 'usuario']")).sendKeys(
				userName);
		webDriver.findElement(By.xpath("//input[@name= 'nombre']")).sendKeys(
				name);
		webDriver.findElement(By.xpath("//input[@name= 'apellido']")).sendKeys(
				lastName);
		webDriver.findElement(By.xpath("//input[@name= 'fechaNacimiento']"))
				.sendKeys(birthday);
		webDriver.findElement(By.xpath("//input[@name = 'email']")).sendKeys(
				email);
	}

}
