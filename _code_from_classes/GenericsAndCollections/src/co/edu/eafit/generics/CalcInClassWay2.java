package co.edu.eafit.generics;

public  class CalcInClassWay2 {
	
	@SuppressWarnings("unchecked")
	public <T extends Number> double add(T a, T b){
		return Double.valueOf(a.doubleValue() + a.doubleValue());
	}

	@SuppressWarnings("unchecked")
	public <T extends Number> double multiply(T a, T b){
		return Double.valueOf(a.doubleValue() * a.doubleValue());
	}
}
