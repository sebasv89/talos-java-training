package co.edu.eafit.generics;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class ReflectionExample {

	public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, SecurityException, NoSuchMethodException, NoSuchFieldException{
//		Method[] methods = Dog.class.getDeclaredMethods();
//		Dog dog = new Dog();
//		
//		Method addMethod = Dog.class.getMethod("add", Integer.class, Integer.class);
//		
//		System.out.println(addMethod.invoke(dog, 1, 2));

		
		Account sebastiansAccount = new Account();
		sebastiansAccount.balance = 10000d;
		sebastiansAccount.id = "1036623487";
		
		System.out.println(extractValueAtRuntime("id", sebastiansAccount));
		
	}
	
	public static Object extractValueAtRuntime(String fieldName, Object instance) throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException{

		Field field = instance.getClass().getField(fieldName);
		Object object = field.get(instance);
		return object;
		
	}
}
