package co.edu.eafit.generics;

public abstract class Animal {

	public abstract void live();
}
