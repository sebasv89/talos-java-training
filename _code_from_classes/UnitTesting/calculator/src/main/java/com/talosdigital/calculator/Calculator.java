package com.talosdigital.calculator;

public class Calculator {

	public int add(int a, int b) {
		return a + b;
	}

	public int divide(int a, int b) {
		return a / b;
	}

	public int toInteger(String value) {
		return Integer.parseInt(value);
	}

	public boolean isPositive(int value) {
		if (value >= 0) {
			return true;
		}
		return false;
	}
}
