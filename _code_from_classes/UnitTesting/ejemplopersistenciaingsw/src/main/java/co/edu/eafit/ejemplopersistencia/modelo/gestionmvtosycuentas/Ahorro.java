package co.edu.eafit.ejemplopersistencia.modelo.gestionmvtosycuentas;

import java.util.Date;

import co.edu.eafit.ejemplopersistencia.dao.Entidad;
import co.edu.eafit.ejemplopersistencia.modelo.gestioncliente.Cliente;



public class Ahorro extends Cuenta
{
	//Atributos
    private float interes;

    public Ahorro(){

    }

    public Ahorro(String intfechaApertura, Cliente cliente, int saldo, int interes, int idCuenta)
    {
		super(intfechaApertura, cliente, saldo, idCuenta);
		this.interes = interes;
	}

    public boolean esIgual(Entidad entidad) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public float getInteres() {
        return interes;
    }

    public void setInteres(float interes) {
        this.interes = interes;
    }
   
}
