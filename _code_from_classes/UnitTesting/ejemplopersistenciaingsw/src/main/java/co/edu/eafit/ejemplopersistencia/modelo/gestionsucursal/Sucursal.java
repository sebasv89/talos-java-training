package co.edu.eafit.ejemplopersistencia.modelo.gestionsucursal;

import java.util.*;

import co.edu.eafit.ejemplopersistencia.modelo.gestionmvtosycuentas.*;

public class Sucursal
{
    //Atributos
    private int idSucursal;
    private String nombre;
    private ArrayList <Cuenta> cuentas;
    private ArrayList <PuntosAtencion> puntosAtencion; 
    
    public Sucursal (int idSucursal, String nombre)
    {
    	this.idSucursal = idSucursal;
    	this.nombre = nombre;
    	cuentas = new ArrayList <Cuenta>();
    	puntosAtencion = new ArrayList <PuntosAtencion>();
    }
    
    public void addCuenta (Cuenta c)
    {
    	cuentas.add(c);
    }
    public void addPunto (PuntosAtencion  p)
    {
    	puntosAtencion.add(p);
    }
}
