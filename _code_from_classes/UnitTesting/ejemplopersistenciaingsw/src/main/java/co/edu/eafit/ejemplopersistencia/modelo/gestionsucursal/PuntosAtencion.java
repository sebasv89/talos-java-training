package co.edu.eafit.ejemplopersistencia.modelo.gestionsucursal;

import co.edu.eafit.ejemplopersistencia.dao.Entidad;


public class PuntosAtencion implements Entidad
{
    //Atributos
    protected int numOperRealizadas;
    
    public PuntosAtencion ()
    {
    	this.numOperRealizadas = 0;
    }
    
    public void addOperacion ()
    {
    	numOperRealizadas ++;
    }

    public boolean esIgual(Entidad entidad) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
