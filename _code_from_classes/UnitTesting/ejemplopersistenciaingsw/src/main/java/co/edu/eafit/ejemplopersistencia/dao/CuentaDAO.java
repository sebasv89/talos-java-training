/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package co.edu.eafit.ejemplopersistencia.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import co.edu.eafit.ejemplopersistencia.modelo.gestioncliente.Cliente;
import co.edu.eafit.ejemplopersistencia.modelo.gestionmvtosycuentas.Ahorro;
import co.edu.eafit.ejemplopersistencia.modelo.gestionmvtosycuentas.Corriente;
import co.edu.eafit.ejemplopersistencia.modelo.gestionmvtosycuentas.Cuenta;
import co.edu.eafit.ejemplopersistencia.modelo.gestionmvtosycuentas.MvtosCuenta;

import com.mysql.jdbc.Connection;

/**
 * 
 * @author raquel
 */
public class CuentaDAO implements DaoInterface {

	@Override
	public int insert(Entidad entidad) throws SQLException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public int update(Entidad bean, String condicion) throws SQLException {
		Cuenta cta = (Cuenta) bean;
		Connection con;
		try {
			con = JDBCConnection.getConexion();
			String sqlStm = "update cuenta set saldo = " + cta.getSaldo()
					+ " where idCuenta = " + cta.getIdCuenta();
			java.sql.Statement sqlSta = con.createStatement();
			sqlSta.executeUpdate(sqlStm);

			// si la cuenta tiene movimientos nuevos los ingresa tambien a la
			// base de datos

			ArrayList mvtos = cta.getEntradas();
			int i = 0;
			while (i < mvtos.size()) {
				MvtosCuenta mc = (MvtosCuenta) mvtos.get(i);
				if (mc.esNuevo()) {
					insertarMovimiento(cta, mc);
				}
				i++;
			}
		} catch (Exception ex) {
			Logger.getLogger(CuentaDAO.class.getName()).log(Level.SEVERE, null,
					ex);

		}

		return 0;

	}

	/**
	 * Busqueda por clave primaria
	 * 
	 * @param codigo
	 *            = numero de la cuenta
	 * @return
	 * @throws SQLException
	 */
	@Override
	public Entidad find(int codigo) throws SQLException {
		Connection con;
		try {
			con = JDBCConnection.getConexion();
			String orden = "SELECT * FROM cuenta c " + "WHERE c.idCuenta = "
					+ codigo;
			java.sql.Statement sentencia = con.createStatement();
			ResultSet rs = sentencia.executeQuery(orden);
			System.out.println("ejecuto query de cuenta");
			Cuenta cuenta = null;
			int idCliente = -1;
			while (rs.next()) {
				System.out.println("dentro del ciclo");
				int tipo = rs.getInt("tipoCuenta");
				idCliente = rs.getInt("Cliente_idCliente");

				if (tipo == 1) {
					Corriente ctaCorriente = new Corriente();
					System.out.println("creo cuenta correinete");

					ctaCorriente.setTopeSobrejiro(rs.getInt("topeSobregiro"));
					ctaCorriente.setIdCuenta(rs.getInt("idCuenta"));
					ctaCorriente.setSaldo(rs.getInt("saldo"));
					ctaCorriente.setIntfechaApertura(rs
							.getString("fechaApertura"));
					ctaCorriente.setCliente(recuperarCliente(idCliente));
					cuenta = ctaCorriente;

				} else if (tipo == 2) {
					Ahorro ctaAhorro = new Ahorro();
					ctaAhorro.setInteres(rs.getFloat("interes"));
					ctaAhorro.setIdCuenta(rs.getInt("idCuenta"));
					ctaAhorro.setSaldo(rs.getInt("saldo"));
					ctaAhorro
							.setIntfechaApertura(rs.getString("fechaApertura"));
					ctaAhorro.setCliente(recuperarCliente(idCliente));
					cuenta = ctaAhorro;
				}
			}
			sentencia.close();
			// JDBCConnection.cerrarConexion();
			// establece la conexion de la cuenta con el cliente
			return cuenta;
		} catch (Exception ex) {
			Logger.getLogger(CuentaDAO.class.getName()).log(Level.SEVERE, null,
					ex);
			return null;
		}

		// // Si la condición es null o vacia, no hay parte WHERE

	}

	@Override
	public Vector select(String condicion) throws SQLException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public int delete(String condicion) throws SQLException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public Vector selectIterator(String condicion) throws SQLException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	/**
	 * Recupera el cliente de la cuenta
	 * 
	 * @param idCliente
	 * @return
	 */
	private Cliente recuperarCliente(int idCliente) throws SQLException {
		DaoInterface daoEntidad = new FactoriaDAO().getDaoCliente();
		return (Cliente) daoEntidad.find(idCliente);
	}

	private void insertarMovimiento(Cuenta cta, MvtosCuenta mc)
			throws SQLException {
		try {
			Connection con = JDBCConnection.getConexion();
			String orden = "INSERT INTO movimientoscuenta"
					+ " (Cuenta_idCuenta, tipoMovimiento, valorMovimiento, fechaMovimiento) VALUES ("
					+ +cta.getIdCuenta() + ", " + mc.getTipoMvto() + ", "
					+ mc.getValorMvto() + ",'" + mc.getFechaMvto() + "')";
			System.out.println("comando para ejecutar " + orden);
			java.sql.Statement sentencia = con.createStatement();
			int numFilas = sentencia.executeUpdate(orden);
			sentencia.close();
		} catch (Exception ex) {
			Logger.getLogger(CuentaDAO.class.getName()).log(Level.SEVERE, null,
					ex);
		}

	}

}
