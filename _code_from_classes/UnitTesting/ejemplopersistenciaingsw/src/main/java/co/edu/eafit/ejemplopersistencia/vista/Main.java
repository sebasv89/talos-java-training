/*
 * Main.java
 *
 * Created on 21 de septiembre de 2009, 17:22
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package co.edu.eafit.ejemplopersistencia.vista;
import javax.swing.UIManager;

import co.edu.eafit.ejemplopersistencia.modelo.gestioncliente.*;
import co.edu.eafit.ejemplopersistencia.modelo.gestionmvtosycuentas.*;

/**
 *
 * @author lellisga
 */
public class Main {
    
    /** Creates a new instance of Main */
    public Main() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (Exception e){}
        //Abre ventana principal
        VentanaTransferencia ventana = new VentanaTransferencia();
        ventana.setVisible(true);
    }
    
}
