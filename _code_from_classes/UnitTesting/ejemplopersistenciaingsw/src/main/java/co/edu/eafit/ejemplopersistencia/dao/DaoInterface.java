/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package co.edu.eafit.ejemplopersistencia.dao;

import java.sql.SQLException;
import java.util.Vector;

/**
 * 
 * @author raquel
 */
public interface DaoInterface {

	public int insert(Entidad entidad) throws SQLException;

	public int update(Entidad bean, String condicion) throws SQLException;

	public Entidad find(int codigo) throws SQLException;

	public Vector select(String condicion) throws SQLException;

	public int delete(String condicion) throws SQLException;

	public Vector selectIterator(String condicion) throws SQLException;
}
