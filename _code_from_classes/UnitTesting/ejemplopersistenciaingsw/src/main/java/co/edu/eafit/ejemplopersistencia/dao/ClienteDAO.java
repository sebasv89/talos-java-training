/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package co.edu.eafit.ejemplopersistencia.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import co.edu.eafit.ejemplopersistencia.modelo.gestioncliente.Cliente;

import com.mysql.jdbc.Connection;

/**
 * 
 * 
 * @author raquel
 */
public class ClienteDAO implements DaoInterface {

	@Override
	public int insert(Entidad entidad) throws SQLException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public int update(Entidad bean, String condicion) throws SQLException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public Entidad find(int codigo) throws SQLException {
		try {
			Connection con = JDBCConnection.getConexion();
			// // Si la condición es null o vacia, no hay parte WHERE
			String orden = "SELECT * FROM cliente cl "
					+ "WHERE cl.idcliente = " + codigo;
			java.sql.Statement sentencia = con.createStatement();
			ResultSet rs = sentencia.executeQuery(orden);
			Cliente cliente = null;
			while (rs.next()) {
				cliente = new Cliente();
				cliente.setIdCliente(rs.getInt("idcliente"));
				cliente.setNombre(rs.getString("nombre"));
				cliente.setApellido(rs.getString("apellido"));
				System.out.println("recupero cliente " + cliente);
			}
			sentencia.close();
			return cliente;
		} catch (Exception ex) {
			Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE,
					null, ex);
			return null;
		}
	}

	@Override
	public Vector select(String condicion) throws SQLException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public int delete(String condicion) throws SQLException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public Vector selectIterator(String condicion) throws SQLException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

}
