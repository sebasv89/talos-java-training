package co.edu.eafit.ejemplopersistencia.control;

import static org.mockito.Mockito.*;

import java.sql.SQLException;
import java.util.Observer;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import co.edu.eafit.ejemplopersistencia.dao.DaoInterface;
import co.edu.eafit.ejemplopersistencia.dao.FactoriaDAO;
import co.edu.eafit.ejemplopersistencia.modelo.gestioncliente.Cliente;
import co.edu.eafit.ejemplopersistencia.modelo.gestionmvtosycuentas.Cuenta;

@RunWith(MockitoJUnitRunner.class)
public class TransferirFondosControllerTest {

	private static final int CLIENT_ID = 1;
	private static final int SOURCE_ACCOUNT = 4;
	private static final int DESTINATION_ACCOUNT = 5;

	TransferirFondosCtrl controller = new TransferirFondosCtrl();

	@Mock
	private FactoriaDAO daoFactory;

	@Mock
	private DaoInterface daoClient;

	@Mock
	private DaoInterface daoCuenta;

	@Mock
	private Cliente client;

	@Mock
	private Cuenta sourceAccount, destinationAccount;

	@Mock
	private Observer vista;

	@Before
	public void init() throws SQLException {
		when(daoFactory.getDaoCliente()).thenReturn(daoClient);
		when(daoFactory.getDaoCuenta()).thenReturn(daoCuenta);
		controller.setFactoriaDao(daoFactory);
		controller.setObserver(vista);
	}

	@Test
	public void testTransfer() throws Exception {
		when(daoClient.find(CLIENT_ID)).thenReturn(client);
		when(daoCuenta.find(SOURCE_ACCOUNT)).thenReturn(sourceAccount);
		when(daoCuenta.find(DESTINATION_ACCOUNT))
				.thenReturn(destinationAccount);
		when(sourceAccount.getCliente()).thenReturn(client);
		when(client.getId()).thenReturn(CLIENT_ID);
		String transferResult = controller.transferir(CLIENT_ID,
				SOURCE_ACCOUNT, DESTINATION_ACCOUNT, 3212);
		Assert.assertEquals("Transferencia Exitosa", transferResult);
		Mockito.verify(sourceAccount).addObserver(vista);

	}

}
