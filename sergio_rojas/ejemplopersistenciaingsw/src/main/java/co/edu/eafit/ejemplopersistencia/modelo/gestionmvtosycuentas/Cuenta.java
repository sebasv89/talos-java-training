package co.edu.eafit.ejemplopersistencia.modelo.gestionmvtosycuentas;

import java.util.*;

import co.edu.eafit.ejemplopersistencia.dao.Entidad;
import co.edu.eafit.ejemplopersistencia.excepcion.SaldoInsuficienteExcepcion;
import co.edu.eafit.ejemplopersistencia.modelo.gestioncliente.Cliente;


public abstract class Cuenta extends Observable implements Entidad
{
    //Atributos
    protected String intfechaApertura;
    protected int saldo;
    protected ArrayList <MvtosCuenta> movimientos;
    protected Cliente cliente;
    protected int idCuenta;
    
    //Constructor invocado por los servicios de DAO
    public Cuenta(){
        movimientos = new ArrayList <MvtosCuenta> ();
    }
    // constructor invocado cuando se crea una nueva cuenta
    public Cuenta (String intfechaApertura, Cliente cliente, int saldo, int idCuenta)
    {
    	this.intfechaApertura = intfechaApertura;
    	this.cliente = cliente;
    	this.saldo = saldo;
    	this.idCuenta = idCuenta;
    	movimientos = new ArrayList <MvtosCuenta> ();
    }
    
    /*
     * M�todo para consignar un una cantidad x a la cuenta
     */
    
    public void consignar (int x)
    {
          System.out.println("entra a cnsignar ");
    	saldo = saldo + x;
        addMvtoCuenta(MvtosCuenta.ENTRADA, x);
    }

    /*
     * M�todo que agrega un Nuevo Movimiento creado por primera vez
     */
    protected void addMvtoCuenta (int tipo, int valor)
    {
        MvtosCuenta mv = new MvtosCuenta(tipo, valor, Calendar.getInstance().getTime());
    	movimientos.add(mv);
        mv.setNuevo(true);
        System.out.println("se agrego un movimiento");
        // realiza a los observadores la notificacion de cambio del modelo
        super.setChanged();
        notifyObservers();
    }
    
    /*
     * M�todo para retirar la cantidad x de la cuenta
     */
    public void retirar (int x) throws Exception 
    {
        System.out.println("entra a retirar ");
    	saldo = saldo - x;
    	if (saldo < 0)
    	{	
    		saldo = saldo + x;
    		throw new SaldoInsuficienteExcepcion ("Saldo Insuficiente");
    	} else{
            
        addMvtoCuenta(MvtosCuenta.SALIDA, x);
       

          }
    }
    public int getId ()
    {
    	return idCuenta;
    }
    
    public int getSaldo ()
    {
    	return saldo;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public ArrayList<MvtosCuenta> getEntradas() {
        return movimientos;
    }

    public void setEntradas(ArrayList<MvtosCuenta> entradas) {
        this.movimientos = entradas;
    }

    public int getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(int idCuenta) {
        this.idCuenta = idCuenta;
    }

    public String getIntfechaApertura() {
        return intfechaApertura;
    }

    public void setIntfechaApertura(String intfechaApertura) {
        this.intfechaApertura = intfechaApertura;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }
    public boolean esIgual( Entidad entidad ){
        return true;
    }
}
