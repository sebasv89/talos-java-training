/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package co.edu.eafit.ejemplopersistencia.excepcion;

/**
 *
 * @author raquel
 */
public class CuentaInexistenteExcepcion extends Exception{

    public CuentaInexistenteExcepcion(){
        super();
    }

    public CuentaInexistenteExcepcion(String string) {
        super(string);
    }
}
