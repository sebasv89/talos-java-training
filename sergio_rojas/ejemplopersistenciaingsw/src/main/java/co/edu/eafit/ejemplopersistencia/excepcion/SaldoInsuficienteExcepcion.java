/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package co.edu.eafit.ejemplopersistencia.excepcion;

/**
 *
 * @author raquel
 */
public class SaldoInsuficienteExcepcion extends Exception {

    public SaldoInsuficienteExcepcion(){
        super();
    }

    public SaldoInsuficienteExcepcion(String string) {
       super(string);
    }
}
