/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package co.edu.eafit.ejemplopersistencia.excepcion;

/**
 *
 * @author eafit
 */
public class ClienteInexistenteExcepcion extends Exception {
  public ClienteInexistenteExcepcion(String string) {
        super(string);
    }
}
