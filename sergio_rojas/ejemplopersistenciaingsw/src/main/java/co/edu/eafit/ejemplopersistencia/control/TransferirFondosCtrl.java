package co.edu.eafit.ejemplopersistencia.control;

import java.util.Observer;

import co.edu.eafit.ejemplopersistencia.dao.DaoInterface;
import co.edu.eafit.ejemplopersistencia.dao.FactoriaDAO;
import co.edu.eafit.ejemplopersistencia.excepcion.ClienteInexistenteExcepcion;
import co.edu.eafit.ejemplopersistencia.excepcion.ClienteInvalidoExcepcion;
import co.edu.eafit.ejemplopersistencia.excepcion.CuentaInexistenteExcepcion;
import co.edu.eafit.ejemplopersistencia.modelo.gestioncliente.Cliente;
import co.edu.eafit.ejemplopersistencia.modelo.gestionmvtosycuentas.Cuenta;

public class TransferirFondosCtrl {
	Observer vista;

	private FactoriaDAO factoriaDao = new FactoriaDAO();

	// Constructor
	public TransferirFondosCtrl() {
	}

	public String transferir(int cliente, int idCuentaDesde, int idCuentaHasta,
			int cantidad) throws Exception {
		// try{
		DaoInterface daoEntidad = factoriaDao.getDaoCliente();
		Cliente clienteEnt = (Cliente) daoEntidad.find(cliente);
		if (clienteEnt == null) {
			throw new ClienteInexistenteExcepcion("El cliente " + cliente
					+ " no existe");
		}
		daoEntidad = factoriaDao.getDaoCuenta();
		Cuenta cuentaDesde = (Cuenta) daoEntidad.find(idCuentaDesde);

		// chequeando precondiciones
		if (cuentaDesde == null) {
			throw new CuentaInexistenteExcepcion("La cuenta desde "
					+ idCuentaDesde + " no existe");
		}
		// asocia al elemento recuperado su observador
		cuentaDesde.addObserver(vista);

		Cuenta cuentaHasta = (Cuenta) daoEntidad.find(idCuentaHasta);

		if (cuentaHasta == null) {
			throw new CuentaInexistenteExcepcion("La cuenta hasta "
					+ idCuentaHasta + " no existe");
		}
		// asocia al elemento recuperado su observador
		cuentaHasta.addObserver(vista);
		if (clienteEsValido(cliente, cuentaDesde)) {
			System.out.println("va a actualizar");
			cuentaDesde.retirar(cantidad);
			cuentaHasta.consignar(cantidad);
			// invoca los servicios para actualizacion en la base de datos
			daoEntidad.update(cuentaDesde, null);
			daoEntidad.update(cuentaHasta, null);
			return "Transferencia Exitosa";
		} else {
			throw new ClienteInvalidoExcepcion(
					"El cliente que hace la transferencia  " + cliente
							+ " no es propietario de la cuenta de origen "
							+ idCuentaDesde);
		}

	}

	// verifica que cliente que va a hacer el movimiento sea el dueño de la
	// cuenta

	private boolean clienteEsValido(int cliente, Cuenta cuenta) {
		if (cuenta.getCliente().getId() == cliente)
			return true;
		else {
			System.out.println("Cliente Invalido");
			return false;
		}
	}

	public void setObserver(Observer ventana) {
		vista = ventana;

	}

	public void setFactoriaDao(FactoriaDAO factoriaDao) {
		this.factoriaDao = factoriaDao;
	}

}
