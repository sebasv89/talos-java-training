/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package co.edu.eafit.ejemplopersistencia.excepcion;

/**
 *
 * @author raquel
 */
public class ClienteInvalidoExcepcion extends Exception {

    public ClienteInvalidoExcepcion(String string) {
        super(string);
    }

}
