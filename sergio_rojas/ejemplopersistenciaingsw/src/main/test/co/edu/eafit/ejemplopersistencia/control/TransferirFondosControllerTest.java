package co.edu.eafit.ejemplopersistencia.control;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.sql.SQLException;
import java.util.Observer;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import co.edu.eafit.ejemplopersistencia.dao.DaoInterface;
import co.edu.eafit.ejemplopersistencia.dao.FactoriaDAO;
import co.edu.eafit.ejemplopersistencia.excepcion.ClienteInexistenteExcepcion;
import co.edu.eafit.ejemplopersistencia.excepcion.CuentaInexistenteExcepcion;
import co.edu.eafit.ejemplopersistencia.modelo.gestioncliente.Cliente;
import co.edu.eafit.ejemplopersistencia.modelo.gestionmvtosycuentas.Cuenta;

@RunWith(MockitoJUnitRunner.class)
public class TransferirFondosControllerTest {

	private static final int CLIENT_ID = 1;
	private static final int ACCOUNT_NUM = 2;

	TransferirFondosCtrl controller = new TransferirFondosCtrl();

	@Mock
	private FactoriaDAO daoFactory;

	@Mock
	private DaoInterface daoClient;
	
	@Mock
	private DaoInterface daoCuenta;

	@Mock
	private Cliente client;
	
	@Mock
	private Cuenta cuenta;
	
	@Mock
	private Observer vista;

	@Before
	public void init() throws SQLException {
		when(daoFactory.getDaoCliente()).thenReturn(daoClient);
		when(daoFactory.getDaoCuenta()).thenReturn(daoCuenta);
		controller.setFactoriaDao(daoFactory);
		controller.setObserver(vista);
	}

	@Test(expected = ClienteInexistenteExcepcion.class)
	public void testTransferWithNonExistentClient() throws Exception {
		when(daoClient.find(CLIENT_ID)).thenReturn(null);

		controller.transferir(CLIENT_ID, 4, 5, 3212);
	}
	
	@Test(expected = CuentaInexistenteExcepcion.class)
	public void testCuentaInexistente() throws Exception{
		when(daoClient.find(CLIENT_ID)).thenReturn(client);
		when(daoCuenta.find(ACCOUNT_NUM)).thenReturn(null);
		
		controller.transferir(CLIENT_ID, 4, 5, 3212);
	}
	
	@Test
	public void testTransferenciaExitosa() throws Exception{
		when(daoClient.find(CLIENT_ID)).thenReturn(client);
		when(daoCuenta.find(ACCOUNT_NUM)).thenReturn(cuenta);
		when(cuenta.getCliente()).thenReturn(client);
		when(client.getId()).thenReturn(CLIENT_ID);
	
		String mensaje = controller.transferir(CLIENT_ID, ACCOUNT_NUM, ACCOUNT_NUM, 3212);
		assertEquals(mensaje,"Transferencia Exitosa");
	}

}