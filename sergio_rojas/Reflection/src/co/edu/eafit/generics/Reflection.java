/**
 * 
 */
package co.edu.eafit.generics;

/**
 * @author checho
 *
 */

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class Reflection {

	/**
	 * @param args
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	
	public static void printValues(Object instance) throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException{
		System.out.println("------------------------------------------------");
		System.out.println("Loggin a " + instance.getClass().getSimpleName() + " " + "object");		
		for(Field field : instance.getClass().getDeclaredFields()){
			field.setAccessible(true);			
			System.out.println(field.getName() + "-" + field.getType().getSimpleName() + ": Value = " + field.get(instance));			
		}
		System.out.println("------------------------------------------------");
	}
	
	public static void main(String[] args) throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException {
		// TODO Auto-generated method stub
		Person person = new Person();
		person.setFirstName("Sergio");
		person.setLastName("Rojas");
		person.setAge(20);
				
		printValues(person);
	}

}
