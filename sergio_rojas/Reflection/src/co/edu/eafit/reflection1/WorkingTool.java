/**
 * 
 */
package co.edu.eafit.reflection1;

/**
 * @author checho
 *
 */
public class WorkingTool {
	
	private int toolNumber;
	private String toolName;
	
	public int getToolNumber() {
		return toolNumber;
	}
	public void setToolNumber(int toolNumber) {
		this.toolNumber = toolNumber;
	}
	public String getToolName() {
		return toolName;
	}
	public void setToolName(String toolName) {
		this.toolName = toolName;
	}	
	
}
