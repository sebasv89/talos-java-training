/**
 * 
 */
package co.edu.eafit.reflection1;

/**
 * @author checho
 *
 */
public class Employee {
	
	private String firstname;
	private String lastName;
	private int age;
	private WorkingTool tool;
	
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public WorkingTool getTool() {
		return tool;
	}
	public void setTool(WorkingTool tool) {
		this.tool = tool;
	}
	
}
