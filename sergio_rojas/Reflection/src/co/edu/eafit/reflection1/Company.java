/**
 * 
 */
package co.edu.eafit.reflection1;

/**
 * @author checho
 *
 */
public class Company {
	
	private String companyName;
	private int numericCode;
	private Employee employee;
	
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public int getNumericCode() {
		return numericCode;
	}
	public void setNumericCode(int numericCode) {
		this.numericCode = numericCode;
	}
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}	
}
