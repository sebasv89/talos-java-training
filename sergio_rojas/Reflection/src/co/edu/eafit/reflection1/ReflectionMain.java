/**
 * 
 */
package co.edu.eafit.reflection1;

/**
 * @author checho
 *
 */
import java.lang.reflect.Field;

public class ReflectionMain {

	/**
	 * @param args
	 */
	private static String [] iterationArray = new String [] {"String", "int"};
	
	public static void printValues(Object instance, int indentation) throws IllegalArgumentException, IllegalAccessException{
		System.out.printf("%" + indentation + "s" + "------------------------------------------------------- \n", "");
		System.out.printf("%" + indentation + "s" + " Loggin a " + instance.getClass().getSimpleName() + " " + "object \n", "");
		for(Field field : instance.getClass().getDeclaredFields()){
			field.setAccessible(true);
			if(field.getType().getSimpleName().equals(iterationArray[0]) || field.getType().getSimpleName().equals(iterationArray[1])){
				System.out.printf("%" + indentation + "s" + field.getName() + "-" + field.getType().getSimpleName() + ": Value = " + field.get(instance) + "\n", "");
			}else{
				printValues(field.get(instance), indentation+4);
			}
		}
		System.out.printf("%" + indentation + "s" + "------------------------------------------------------- \n", "");
	}
	
	public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException {
		// TODO Auto-generated method stub
		WorkingTool tool = new WorkingTool();
		tool.setToolNumber(87);
		tool.setToolName("Computer");
		
		Employee employee = new Employee();
		employee.setFirstname("Sergio");
		employee.setLastName("Rojas");
		employee.setAge(20);
		employee.setTool(tool);
		
		Company company = new Company();
		company.setCompanyName("EAFIT");
		company.setNumericCode(2020);
		company.setEmployee(employee);

		printValues(company, 2);
	}

}
