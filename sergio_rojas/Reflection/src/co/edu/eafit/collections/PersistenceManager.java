/**
 * 
 */
package co.edu.eafit.collections;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * @author checho
 *
 */
public class PersistenceManager {
	
	private static Set<Animal> animalsHashSet = new HashSet<Animal>();	
	private static List<Animal> animalsArrayList = new ArrayList<Animal>();
	private static List<Animal> animalsLinkedList = new LinkedList<Animal>();
	private static Map<Integer, Animal> animalsHashMap = new HashMap<Integer, Animal>();
	private static Map<Integer, Animal> animalsTreeMap = new TreeMap<Integer, Animal>();	
	
	public static void addAnimal(Animal animal){
		// HashSet
		animalsHashSet.add(animal);
		
		// ArrayList
		animalsArrayList.add(animal);
		
		// LinkedList
		animalsLinkedList.add(animal);
		
		// HashMap
		animalsHashMap.put(animal.getId(), animal);
		
		// TreeMap
		animalsTreeMap.put(animal.getId(), animal);
	}
	
	public static List<Animal> getAllAnimals(){
		return animalsLinkedList;
	}
	
	public static Animal getUniqueAnimal(int key){		
		return animalsHashMap.get(key);
	}
	
	public static List<Dog> getAllDogs(){		
		List<Dog> dogList = new LinkedList<Dog>();
		for(Animal animal : animalsLinkedList){
			if(animal instanceof Dog)
				dogList.add((Dog)animal);						
		}			
		return dogList;
	}
	
	public static Animal getAnimalByID(int key){		
		return animalsTreeMap.get(key);
	}
	
	public static Dog getDogByDogName(String dogName){		
		Map<String, Dog> dogMap = new HashMap<String, Dog>();
		for(Animal animal : animalsArrayList){
			if(animal instanceof Dog)
				dogMap.put(((Dog) animal).getDogName(), (Dog)animal);
		}
		return dogMap.get(dogName);
	}
	
	public static Animal getOldestAnimalAndRemove(){
		LinkedList<Animal> queue = new LinkedList<Animal>();
		for(Animal animal : animalsLinkedList){
			queue.addFirst(animal);
		}		
		return queue.removeLast();
	}
	
	public static void main(String [] args){		
		Dog dog = new Dog();
		dog.setId(10);
		dog.setDogName("Sergio");
		addAnimal(dog);
		
		Animal dog2 = new Dog();
		dog2.setId(11);		
		addAnimal(dog2);
		
		System.out.println("ID AnimalByID: " + getAnimalByID(11).getId());
		System.out.println("ID OldestAnimal: " + getOldestAnimalAndRemove().getId());
		System.out.println("DogByName: " + getDogByDogName("Sergio").getDogName());
		
	}

}
