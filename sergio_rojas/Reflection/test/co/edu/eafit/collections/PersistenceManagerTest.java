package co.edu.eafit.collections;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

public class PersistenceManagerTest {

	@Mock
	PersistenceManager persistenceManager = new PersistenceManager();
	
	Animal animal = new Dog();
	
	
	@Before
	public void before(){
		animal.setId(0);
		persistenceManager.addAnimal(animal);
	}
	
	@Test
	public void testGetAllAnimals() {
		List<Animal> animals = new ArrayList<Animal>();		
		animals = persistenceManager.getAllAnimals();
		
		assertTrue("List is empty", !animals.isEmpty());
	}
	
	@Test(expected = NullPointerException.class)
	public void testGetUniqueAnimalNotFound(){
		animal = persistenceManager.getUniqueAnimal(3);
		
		assertEquals(0, animal.getId());
	}
	
	@Test
	public void testGetUniqueAnimal(){
		animal = persistenceManager.getUniqueAnimal(0);
		
		assertEquals(0, animal.getId());
	}
	
	@Test
	public void testGetAllDogs(){
		List<Dog> dogList = new LinkedList<Dog>();
		dogList = persistenceManager.getAllDogs();
		
		assertTrue("DogList is empty", !dogList.isEmpty());		
	}
	
	@Test
	public void testGetAnimalByID(){
		animal= persistenceManager.getAnimalByID(0);
		
		assertEquals(0, animal.getId());
	}
	
	@Test
	public void testGetDogByName(){
		Dog dog = new Dog();
		dog.setDogName("Sergio");
		persistenceManager.addAnimal(dog);
		dog = persistenceManager.getDogByDogName("Sergio");
		
		assertEquals("Sergio", dog.getDogName());
	}
	
	@Test
	public void testGetOldestAnimalAndRemove(){
		Animal animal2 = new Dog();
		animal2.setId(1);
		persistenceManager.addAnimal(animal2);
		
		Animal oldestAnimal = persistenceManager.getOldestAnimalAndRemove();
		
		assertEquals(0, oldestAnimal.getId());
	}

}
