package co.edu.eafit.day1.simplereflection;

/**
 *
 * @author Sebastian
 */
public class Person {

    public String firstName;
    public String lastName;
    private String telephone;

    /**
     * Setting a simple person data
     * @param firstName
     * @param lastName
     * @param telephone 
     */
    public Person(String firstName, String lastName, String telephone) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.telephone = telephone;
    }
}
