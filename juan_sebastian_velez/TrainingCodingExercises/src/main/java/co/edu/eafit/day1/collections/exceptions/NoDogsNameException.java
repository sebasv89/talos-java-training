package co.edu.eafit.day1.collections.exceptions;

public class NoDogsNameException extends Exception{
	public NoDogsNameException(){
		super("There is not an dog with that name");
	}
}
