package co.edu.eafit.day2.selenium.test;

import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import co.edu.eafit.day2.selenium.RegisterSalesPoint;
import co.edu.eafit.day2.selenium.RegisterStore;

public class RegisterSalesPointTest {
	@Test
	public void testEmpty() {
		RegisterSalesPoint page = new RegisterSalesPoint();
		page.submit();
		String message = page.getErrorMessage();
		Assert.assertEquals("la direcci�n no puede ser vacia", message);
		page.close();
	}

	@Test
	public void testHappyPath() {
		String nit = "12" + new Random().nextInt();
		String storeName = "Machetico";
		
		RegisterStore registerStore = new RegisterStore();
		registerStore.registerStore(nit, storeName);
		registerStore.close();
		
		RegisterSalesPoint page = new RegisterSalesPoint();
		page.fillForm("Hidden Street" + new Random().nextInt(), String.valueOf(new Random().nextInt()), "Fake St 123", "2", "2", nit);
		page.submit();
		String message = page.getSuccessMessage();
		Assert.assertEquals("Punto de venta creado exitosamente", message);
		page.close();
	}

	
	@Test
	public void testDuplicatedUser() {
		String nit = "12" + new Random().nextInt();
		String storeName = "Machetico";
		
		RegisterStore registerStore = new RegisterStore();
		registerStore.registerStore(nit, storeName);
		registerStore.close();
		
		String address = "Hidden Street" + new Random().nextInt();
		String longitude = String.valueOf(new Random().nextInt());
		
		
		RegisterSalesPoint page = new RegisterSalesPoint();

		page.fillForm(address, longitude, "Fake St 123", "2", "2", nit);
		page.submit();
		page.close();

		page = new RegisterSalesPoint();
		page.fillForm(address, longitude, "Fake St 123", "2", "2", nit);
		page.submit();
		String message = page.getNoHandleError();
		Assert.assertEquals("HTTP Status 500 - Request processing failed; nested exception is java.lang.IllegalArgumentException: El punto de venta ya existe: " + address, message);
		page.close();
	}
	
}
