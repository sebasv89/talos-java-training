package co.edu.eafit.day2.selenium.test;

import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import co.edu.eafit.day2.selenium.RegisterStore;

public class RegisterStoreTest {
	@Test
	public void testEmpty() {
		RegisterStore page = new RegisterStore();
		page.submit();
		String message = page.getErrorMessage();
		Assert.assertEquals("El nombre no puede estar vacio", message);
		page.close();
	}

	@Test
	public void testHappyPath() {
		RegisterStore page = new RegisterStore();
		page.fillForm("0123"+new Random().nextInt(), "San andresito");
		page.submit();
		String message = page.getSuccessMessage();
		Assert.assertEquals("Almacen creado exitosamente", message);
		page.close();
	}

	@Test
	public void testDuplicatedUser() {
		RegisterStore page = new RegisterStore();
		String nitAlmacen = "4567" + new Random().nextInt();
		String storeName = "Flamingo";
		page.fillForm(nitAlmacen, storeName);
		page.submit();
		page.close();

		page = new RegisterStore();
		page.fillForm(nitAlmacen, storeName);
		page.submit();
		String message = page.getNoHandleError();
		Assert.assertEquals("HTTP Status 500 - Request processing failed; nested exception is java.lang.IllegalArgumentException: El almacen ya existe: " + storeName, message);
		page.close();
	}
}
