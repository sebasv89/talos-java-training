package co.edu.eafit.day1.recursivereflection;

import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 *
 * @author Sebastian
 */
public class Reflection {

    private static ArrayList<String> dataTypes = new ArrayList<>();

    static {
        dataTypes.add("String");
        dataTypes.add("int");
    }

    public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException {
        Person person = new Person("Sebastian", "Velez", 301445727);
        logObject(person, 0);
    }

    /**
     * Log the attributes (public or private) of an object
     * <p>
     * Example<br>
     * logging a Person object<br>
     * firstName-String: Value = Sebastian<br>
     * lastName-String: Value = Velez
     */
    public static void logObject(Object instance, int level) throws IllegalArgumentException, IllegalAccessException {
        String tab = "";
        for (int i = 0; i < level; i++) {
            tab += "\t";
        }
        System.out.println(tab + "logging a " + instance.getClass().getSimpleName() + " object");
        for (Field field : instance.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            if (!dataTypes.contains(field.getType().getSimpleName())) {
                logObject(field.get(instance), ++level);
                level--;
                continue;
            }

            String fieldName = field.getName();
            String fieldType = field.getType().getSimpleName();
            String fieldValue = field.get(instance).toString();
            System.out.println(tab + fieldName + "-" + fieldType + ": Value = " + fieldValue);
        }
    }
}
