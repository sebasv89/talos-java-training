package co.edu.eafit.day1.collections;

/**
 *
 * @author Sebastian
 */
public class Main {
    public static void main(String[] args) {
        Animal dog = new Dog(1, "Dog");
        Dog scooby = new Dog(2, "Scooby");
        Dog doo = new Dog(3, "Doo");
        Dog doo1 = new Dog(3, "Doo");

        
        Collections collections = new Collections();
        
        collections.add(dog);
        collections.add(scooby);
        collections.add(doo);
        collections.add(doo1);
        collections.add(doo);
        
        collections.getAllAnimals();
        //collections.getUniqueAnimals();
        //collections.getAllDogs();
        //collections.getAnimalById(1);
        //collections.getDogByDogName("Scooby");
        //collections.getOldestAnimalAndRemove();
        //collections.getOldestAnimalAndRemove();
    }
}
