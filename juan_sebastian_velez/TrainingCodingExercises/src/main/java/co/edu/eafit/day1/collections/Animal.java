package co.edu.eafit.day1.collections;

public abstract class Animal {

    private int id;

    public abstract void live();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
