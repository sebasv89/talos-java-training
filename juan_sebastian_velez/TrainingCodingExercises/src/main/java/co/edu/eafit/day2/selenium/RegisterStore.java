package co.edu.eafit.day2.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class RegisterStore {
	private final WebDriver webDriver;

	public RegisterStore() {
		webDriver = new FirefoxDriver();
		webDriver.get("http://10.0.55.154:8080/safebuy/registrarAlmacen.html");
	}

	public void close() {
		webDriver.close();
	}

	public void submit() {
		WebElement button = webDriver.findElement(By.xpath("//input[@value='Registrar']"));
		button.submit();
	}

	public String getErrorMessage() {
		return webDriver.findElement(By.className("error")).getText();
	}
	
	public String getNoHandleError(){
		return webDriver.findElement(By.xpath("//h1[1]")).getText();
	}

	public String getSuccessMessage() {
		return webDriver.findElement(By.className("success")).getText();
	}

	public void fillForm(String nitAlmacen, String nombreAlmacen) {
		webDriver.findElement(By.xpath("//input[@name= 'nitAlmacen']")).sendKeys(nitAlmacen);
		webDriver.findElement(By.xpath("//input[@name= 'nombreAlmacen']")).sendKeys(nombreAlmacen);
	}
	
	public void registerStore(String nit, String name){
		RegisterStore page = new RegisterStore();
		page.fillForm(nit, name);
		page.submit();
		page.close();
	}
}
