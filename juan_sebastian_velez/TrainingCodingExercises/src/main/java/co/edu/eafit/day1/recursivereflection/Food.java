package co.edu.eafit.day1.recursivereflection;

/**
 *
 * @author Sebastian
 */
public class Food {
    public String name;
    public int value;
    
    public Food(){
        this.name = "Mazamorra";
        this.value = 1000;
    }
    
    public Food(String name, int value){
        this.name = name;
        this.value = value;
    }        
}
