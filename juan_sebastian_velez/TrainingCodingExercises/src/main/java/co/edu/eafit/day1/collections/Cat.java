package co.edu.eafit.day1.collections;

public class Cat extends Mammer {

    private String name;

    @Override
    public void doAMammerThing() {
        System.out.println("Executing Do a mammer thing!!");
    }

    @Override
    public void live() {
        System.out.println(name + " is living!");
    }

    public void anotherCatMethod() {
        System.out.println("Executing another cat method!");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
