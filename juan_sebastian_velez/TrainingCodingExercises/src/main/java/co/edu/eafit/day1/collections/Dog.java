package co.edu.eafit.day1.collections;

public class Dog extends Mammer {

    private String name;
    
    /*
     * Sets the default dog's id
     * Sets the default dog's name
     */
    public Dog(){
        super.setId(9999);
        this.name = "Lulu";
    }
    
    public Dog(int id, String name){
        super.setId(id);
        this.name = name;
    }

    @Override
    public void doAMammerThing() {
        System.out.println("Executing Do a mammer thing!!");
    }

    @Override
    public void live() {
        System.out.println(name + " is living!");
    }

    public void anotherDogMethod() {
        System.out.println("Executing another dog method!");
    }

    public int add(Integer a, Integer b) {
        return a + b;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
