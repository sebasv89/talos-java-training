package co.edu.eafit.day2.selenium;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class RegisterSalesPoint {
	private final WebDriver webDriver;

	public RegisterSalesPoint() {
		webDriver = new FirefoxDriver();
		webDriver.get("http://10.0.55.154:8080/safebuy/registrarPuntoDeVenta.html");
	}

	public void close() {
		webDriver.close();
	}

	public void submit() {
		WebElement button = webDriver.findElement(By.xpath("//input[@value='Registrar']"));
		button.submit();
	}

	public String getErrorMessage() {
		return webDriver.findElement(By.className("error")).getText();
	}
	
	public String getNoHandleError(){
		return webDriver.findElement(By.xpath("//h1[1]")).getText();
	}

	public String getSuccessMessage() {
		return webDriver.findElement(By.className("success")).getText();
	}

	public void fillForm(String name, String longitude, String address, String latitude, String altitude, String nit) {
		webDriver.findElement(By.xpath("//input[@name= 'nombre']")).sendKeys(name);
		webDriver.findElement(By.xpath("//input[@name= 'longitude']")).sendKeys(longitude);
		webDriver.findElement(By.xpath("//input[@name= 'direccion']")).sendKeys(address);
		webDriver.findElement(By.xpath("//input[@name= 'latitude']")).sendKeys(latitude);
		webDriver.findElement(By.xpath("//input[@name= 'altitude']")).sendKeys(altitude);
		webDriver.findElement(By.xpath("//select[@name= 'almacenNit']/option[@value = '"+nit+"']")).click();
	}
	
}
