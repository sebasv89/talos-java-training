package co.edu.eafit.day1.collections;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import co.edu.eafit.day1.collections.exceptions.NoAnimalException;
import co.edu.eafit.day1.collections.exceptions.NoDogsNameException;

/**
 *
 * @author Sebastian
 */
public class Collections {

    private List<Animal> list;
    private Set<Animal> set;
    private Map<Integer, Animal> mapAnimalById;
    private Map<String, Dog> mapDogByName;
    private Queue<Animal> queue;
    private List<Dog> listOfDogs;

    public Collections() {
        list = new ArrayList();
        set = new HashSet();
        mapAnimalById = new HashMap(); 
        mapDogByName = new HashMap(); 
        queue = new ArrayDeque();
        listOfDogs = new ArrayList();
    }

    public void add(Animal animal) {
        list.add(animal);
        set.add(animal);
        mapAnimalById.put(animal.getId(), animal);
        queue.add(animal);
        if(animal instanceof Dog){
            mapDogByName.put(((Dog)animal).getName(), (Dog)animal);
            listOfDogs.add((Dog)animal);
        }
    }

    public Collection<Animal> getAllAnimals() {
        return list;
    }

    public Collection<Animal> getUniqueAnimals() {
        return set;
    }

    public void getAllDogs() {
        for(Dog dog : listOfDogs){
            dog.live();
        }
    }

    public Animal getAnimalById(int id) throws NoAnimalException {
    	Animal animal = mapAnimalById.get(id);
    	if(animal != null){
            return animal; 
        }else{
            throw new NoAnimalException();
        }
    }

    /**
     * I know this is wrong, but i saw this during the testing exercise, 
     * so if i have time later i will fix it
     * @param name
     * @return
     * @throws NoDogsNameException
     */
    public Animal getDogByDogName(String name) throws NoDogsNameException {
    	Animal dog = mapDogByName.get(name);
    	if(dog != null){
    		return dog;
    	}else{
    		throw new NoDogsNameException();
    	}
    }

    public Animal getOldestAnimalAndRemove() {
        Animal animal = queue.remove();
        return animal;
    }

    
    /**
     * Setters And Getters
     */
	public List<Animal> getList() {
		return list;
	}

	public void setList(List<Animal> list) {
		this.list = list;
	}

	public Set<Animal> getSet() {
		return set;
	}

	public void setSet(Set<Animal> set) {
		this.set = set;
	}

	public Map<Integer, Animal> getMapAnimalById() {
		return mapAnimalById;
	}

	public void setMapAnimalById(Map<Integer, Animal> mapAnimalById) {
		this.mapAnimalById = mapAnimalById;
	}

	public Map<String, Dog> getMapDogByName() {
		return mapDogByName;
	}

	public void setMapDogByName(Map<String, Dog> mapDogByName) {
		this.mapDogByName = mapDogByName;
	}

	public Queue<Animal> getQueue() {
		return queue;
	}

	public void setQueue(Queue<Animal> queue) {
		this.queue = queue;
	}

	public List<Dog> getListOfDogs() {
		return listOfDogs;
	}

	public void setListOfDogs(List<Dog> listOfDogs) {
		this.listOfDogs = listOfDogs;
	}
    
    
}
