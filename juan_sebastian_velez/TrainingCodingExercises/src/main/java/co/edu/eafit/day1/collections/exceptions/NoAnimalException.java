package co.edu.eafit.day1.collections.exceptions;

public class NoAnimalException extends Exception{

	public NoAnimalException(){
		super("There is no animal");
	}
}
