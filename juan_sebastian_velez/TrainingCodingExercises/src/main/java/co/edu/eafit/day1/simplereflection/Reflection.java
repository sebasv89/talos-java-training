package co.edu.eafit.day1.simplereflection;

import java.lang.reflect.Field;

/**
 *
 * @author Sebastian
 */
public class Reflection {
    public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException{
        Person person = new Person("Sebastian", "Velez", "301 441 5727");
        logObject(person);
    }
    
    /**
     * Log the attributes (public or private) of an object 
     * <p>
     * Example<br>
     * logging a Person object<br>
     * firstName-String: Value = Sebastian<br>
     * lastName-String: Value = Velez
     */
    public static void logObject(Object instance) throws IllegalArgumentException, IllegalAccessException{
        System.out.println("-----------------");
        System.out.println("logging a " + instance.getClass().getSimpleName() + " object");
        
        for(Field field : instance.getClass().getDeclaredFields()){
            field.setAccessible(true);
            String fieldName = field.getName();
            String fieldType = field.getType().getSimpleName();
            String fieldValue = field.get(instance).toString();
            System.out.println(fieldName + "-" + fieldType + ": Value = " + fieldValue);
        }
        System.out.println("-----------------");
    }
}
