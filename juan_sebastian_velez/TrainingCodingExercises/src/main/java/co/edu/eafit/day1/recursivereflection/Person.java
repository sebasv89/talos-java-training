package co.edu.eafit.day1.recursivereflection;

import com.eafit.jvelezpo.day1.simplereflection.*;

/**
 *
 * @author Sebastian
 */
public class Person {
    private Student student1;
    public String firstName;
    public String lastName;
    private int telephone;
    private Student student;

    /**
     * Setting a simple person data
     * @param firstName
     * @param lastName
     * @param telephone 
     */
    public Person(String firstName, String lastName, int telephone) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.telephone = telephone;
        student = new Student("Pepito", 2);
        student1 = new Student("name", 343);
    }
}
