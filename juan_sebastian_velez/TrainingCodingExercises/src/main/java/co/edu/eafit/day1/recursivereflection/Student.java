package co.edu.eafit.day1.recursivereflection;

/**
 *
 * @author Sebastian
 */
public class Student{
    private String name;
    private int legs;    
    public Food food;
            
    public Student(String name, int legs){
        this.name = name;
        this.legs = legs;
        this.food = new Food();
    }
}
