package co.edu.eafit.ejemplopersistencia.control;

import static org.mockito.Mockito.*;

import java.sql.SQLException;
import java.util.Observer;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import co.edu.eafit.ejemplopersistencia.dao.FactoriaDAO;
import co.edu.eafit.ejemplopersistencia.dao.DaoInterface;
import co.edu.eafit.ejemplopersistencia.excepcion.ClienteInexistenteExcepcion;
import co.edu.eafit.ejemplopersistencia.excepcion.ClienteInvalidoExcepcion;
import co.edu.eafit.ejemplopersistencia.excepcion.CuentaInexistenteExcepcion;
import co.edu.eafit.ejemplopersistencia.modelo.gestioncliente.Cliente;
import co.edu.eafit.ejemplopersistencia.modelo.gestionmvtosycuentas.Cuenta;

@RunWith(MockitoJUnitRunner.class)
public class TransferirFondosControllerTest {

	private static final int CLIENT_ID = 1;
	private static final int CUENTA_DESDE = 2;
	private static final int CUENTA_HASTA = 3;

	TransferirFondosCtrl controller = new TransferirFondosCtrl();
	
	@Mock
	private FactoriaDAO daoFactory;
	
	@Mock
	private Observer vista;

	@Mock
	private DaoInterface daoClient;

	@Mock
	private DaoInterface daoCuenta;
	
	@Mock
	private Cliente client;

	@Mock
	private Cuenta cuentaDesde;
	
	@Mock
	private Cuenta cuentaHasta;
	
	@Before
	public void init() throws SQLException {
		when(daoFactory.getDaoCliente()).thenReturn(daoClient);
		when(daoFactory.getDaoCuenta()).thenReturn(daoCuenta);
		controller.setFactoriaDao(daoFactory);
		controller.setObserver(vista);
	}

	@Test(expected = ClienteInexistenteExcepcion.class)
	public void testTransferWithNonExistentClient() throws Exception {
		when(daoClient.find(CLIENT_ID)).thenReturn(null);

		controller.transferir(CLIENT_ID, CUENTA_DESDE, CUENTA_HASTA, 4);
	}
	
	@Test(expected = CuentaInexistenteExcepcion.class)
	public void testCuentaDesdeInexistente() throws Exception{
		when(daoClient.find(CLIENT_ID)).thenReturn(client);
		when(daoCuenta.find(CUENTA_DESDE)).thenReturn(null);
		controller.transferir(CLIENT_ID, CUENTA_DESDE, CUENTA_HASTA, 4);
	}
	
	@Test(expected = CuentaInexistenteExcepcion.class)
	public void testCuentaHastaInexistente() throws Exception{
		when(daoClient.find(CLIENT_ID)).thenReturn(client);
		when(daoCuenta.find(CUENTA_DESDE)).thenReturn(cuentaDesde);
		when(daoCuenta.find(CUENTA_HASTA)).thenReturn(null);
		
		controller.transferir(CLIENT_ID, CUENTA_DESDE, CUENTA_HASTA, 4);
	}
	
	@Test(expected = ClienteInvalidoExcepcion.class)
	public void testClienteInvalido() throws Exception{
		when(daoClient.find(CLIENT_ID)).thenReturn(client);
		when(daoCuenta.find(CUENTA_DESDE)).thenReturn(cuentaDesde);
		when(daoCuenta.find(CUENTA_HASTA)).thenReturn(cuentaHasta);
		
		when(cuentaDesde.getCliente()).thenReturn(client);
		when(client.getId()).thenReturn(CUENTA_DESDE+1);
		
		controller.transferir(CLIENT_ID, CUENTA_DESDE, CUENTA_HASTA, 4);
	}

	@Test
	public void happyPath() throws Exception{
		when(daoClient.find(CLIENT_ID)).thenReturn(client);
		when(daoCuenta.find(CUENTA_DESDE)).thenReturn(cuentaDesde);
		when(daoCuenta.find(CUENTA_HASTA)).thenReturn(cuentaHasta);
		
		when(cuentaDesde.getCliente()).thenReturn(client);
		when(client.getId()).thenReturn(CLIENT_ID);
		
		Assert.assertEquals(controller.transferir(CLIENT_ID, CUENTA_DESDE, CUENTA_HASTA, 4), "Transferencia Exitosa");
		verify(cuentaDesde).addObserver(vista);
		verify(cuentaDesde).retirar(4);
		verify(cuentaHasta).addObserver(vista);
		verify(cuentaHasta).consignar(4);
		verify(daoCuenta).update(cuentaDesde, null);
		verify(daoCuenta).update(cuentaHasta, null);
	}
}
