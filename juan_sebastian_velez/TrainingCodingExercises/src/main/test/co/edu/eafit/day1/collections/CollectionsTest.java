package co.edu.eafit.day1.collections;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import co.edu.eafit.day1.collections.exceptions.NoAnimalException;
import co.edu.eafit.day1.collections.exceptions.NoDogsNameException;

@RunWith(MockitoJUnitRunner.class)
public class CollectionsTest {
	
	@Mock
	private List<Animal> list;
	
	@Mock
	private Set<Animal> set;
	
	@Mock
	private Map<Integer, Animal> mapAnimalById;
    
	@Mock
	private Map<String, Dog> mapDogByName;
    
	@Mock
	private Queue<Animal> queue;
    
	@Mock
	private List<Dog> listOfDogs;
	
	@Mock
	private Animal animal;
	
	Collections collections;
	
	Animal dog = new Dog();
	Animal anotherDog = new Dog();
	
	@Before
	public void init(){
		dog.setId(55);
		collections = new Collections();
		collections.setList(list);
		collections.setSet(set);
		collections.setMapAnimalById(mapAnimalById);
		collections.setQueue(queue);
		collections.setMapDogByName(mapDogByName);
		collections.setListOfDogs(listOfDogs);
	}
	
	@Test
	public void testAdd(){
		collections.add(animal);
		verify(list).add(animal);
		verify(set).add(animal);
		verify(mapAnimalById).put(animal.getId(), animal);
		verify(queue).add(animal);
		if(animal instanceof Dog){
			verify(mapDogByName).put(((Dog)animal).getName(), (Dog)animal);
			verify(listOfDogs).add((Dog)animal);
		}
		
	}
	
	@Test
	public void testGetAllAnimals(){
		collections = new Collections();
		collections.add(dog);
		
		Collection<Animal> animalCollection = new ArrayList<Animal>();
		animalCollection.add(dog);

		Assert.assertEquals(animalCollection, collections.getAllAnimals());
	}
	
	@Test
	public void testUniqueAnimals(){
		collections = new Collections();
		collections.add(dog);
		collections.add(dog);

		int count = collections.getUniqueAnimals().size();
		Assert.assertEquals(count, 1);
	}
	
	@Test
	public void testAnimalById() throws NoAnimalException{
		collections = new Collections();
		collections.add(dog);

		Assert.assertEquals(dog, collections.getAnimalById(55));
	}
	
	@Test(expected = NoAnimalException.class)
	public void testAnimalByIdFailing() throws NoAnimalException{
		collections = new Collections();
		Assert.assertEquals(dog, collections.getAnimalById(1));
	}
	
	@Test
	public void testDogByDogName() throws NoDogsNameException{
		collections = new Collections();
		collections.add(dog);

		Assert.assertEquals(dog, collections.getDogByDogName("Lulu"));
	}
	
	@Test(expected = NoDogsNameException.class)
	public void testDogByDogNameFailing() throws NoDogsNameException{
		collections = new Collections();
		Assert.assertEquals(dog, collections.getDogByDogName("Lucas"));
	}
	
	@Test
	public void testOldestAnimalAndRemove(){
		collections = new Collections();
		collections.add(dog);
		collections.add(anotherDog);
		
		Animal oldest = collections.getOldestAnimalAndRemove();
		Assert.assertEquals(oldest, dog);
		
		oldest = collections.getOldestAnimalAndRemove();
		Assert.assertEquals(oldest, anotherDog);
	}
}
