
public class CollectionExercise 
{
	public static void main(String[] args) 
	{
		AnimalRepository animalRepository = new AnimalRepository();
		
		Cat catData = new Cat();
		catData.animalID = "001";
		catData.catName = "Tiger";
		animalRepository.add(catData);
		animalRepository.add(catData);
		
		Dog dogData = new Dog();
		dogData.animalID = "002";
		dogData.animalName = "My little dog";
		dogData.dogName = "My little dog";
		animalRepository.add(dogData);
		
		animalRepository.getDogByDogName("My");
	}

}
