import java.lang.reflect.Field;

public class LogginExercise 
{
	public static void main(String[] args) 
	{
		Animal firstAnimal = new Animal();
		firstAnimal.animalID = "1";
		firstAnimal.animalName = "cat";

		Animal secondAnimal = new Animal();
		secondAnimal.animalID = "2";
		secondAnimal.animalName = "dog";

		Person firstPerson = new Person();
		firstPerson.animalID = "3";
		firstPerson.animalName = "Human Being";
		firstPerson.firstName = "Cesar";
		firstPerson.middleName = "Augusto";
		firstPerson.lastName = "Quiroz";
		firstPerson.petData = new Dog();
		firstPerson.petData.animalID = "4";
		firstPerson.petData.dogName = "Magia";
				
		LoggData(firstAnimal);
		LoggData(secondAnimal);
		LoggData(firstPerson);

		LoggData(firstAnimal, 0);
		LoggData(secondAnimal, 0);
		LoggData(firstPerson, 0);
	}

	private static void LoggData(Object instanceObject) 
	{
		String firstMessage = "The object instance is null, so we can not make any reflection of it.";
		if (instanceObject != null) 
		{
			firstMessage = "loggin a %s object";
			System.out.println(String.format(firstMessage, instanceObject
					.getClass().getName()));
			Field[] fieldArray = instanceObject.getClass().getFields();
			String secondMessage = "The object instance does not have any field";
			if (fieldArray.length > 0) 
			{
				secondMessage = "%s-%s: Value = %s";
				String dataTemp;
				for (int fieldPosition = 0; fieldPosition < fieldArray.length; fieldPosition++) 
				{
					try 
					{
						dataTemp = String.format(secondMessage,
								fieldArray[fieldPosition].getName(),
								fieldArray[fieldPosition].getType(),
								fieldArray[fieldPosition].get(instanceObject));

						System.out.println(dataTemp);
					} 
					catch (Exception e) 
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				System.out.println("---------------------------------------\n");
			} 
			else 
			{
				System.out.println(secondMessage);
			}
		} 
		else 
		{
			System.out.println(firstMessage);
		}
	}

	private static void LoggData(Object instanceObject, int indentLength) 
	{
		String[] allowedTypesList = new String[] { "int", "String", "Double" };
		String firstMessage = "The object instance is null, so we can not make any reflection of it.";
		if (instanceObject != null) 
		{
			firstMessage = "loggin a %s object";
			padLeftLn(String.format(firstMessage, instanceObject.getClass()
					.getName()), indentLength);
			Field[] fieldArray = instanceObject.getClass().getFields();
			String secondMessage = "The object instance does not have any field";
			if (fieldArray.length > 0) 
			{
				secondMessage = "%s-%s: Value = %s";
				String dataTemp;
				for (int fieldPosition = 0; fieldPosition < fieldArray.length; fieldPosition++) 
				{
					try 
					{
						if (isAllowedType(fieldArray[fieldPosition].getType(),
								allowedTypesList)) 
						{
							dataTemp = String.format(secondMessage,
									fieldArray[fieldPosition].getName(),
									fieldArray[fieldPosition].getType(),
									fieldArray[fieldPosition]
											.get(instanceObject));

							// System.out.println(dataTemp);
							padLeftLn(dataTemp, indentLength);
						} 
						else 
						{
							LoggData(fieldArray[fieldPosition].get(instanceObject),
									indentLength + 5);
						}
					} 
					catch (Exception e) 
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				System.out.print("---------------------------------------\n");
			} 
			else 
			{
				padLeftLn(secondMessage, indentLength);
			}
		} 
		else 
		{
			padLeftLn(firstMessage, indentLength);
		}
	}

	private static boolean isAllowedType(Class<?> classType,
			String[] allowedTypesList) 
	{
		Boolean validationResult = false;

		if (classType != null) 
		{
			if (!classType.isPrimitive()) 
			{
				String dataTypeTemp = classType.toString();
				for (String allowedType : allowedTypesList) 
				{
					if (dataTypeTemp.contains(allowedType)) 
					{
						validationResult = true;
						break;
					}
				}
			} 
			else 
			{
				validationResult = true;
			}
		}

		return validationResult;
	}

	private static void padLeftLn(String message, int paddingValue) 
	{
		paddingValue = paddingValue + message.length();
		System.out.print(String.format("%" + paddingValue + "s \n", message));
	}
}
