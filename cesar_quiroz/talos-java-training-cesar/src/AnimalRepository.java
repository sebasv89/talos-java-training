import java.util.*;

public class AnimalRepository 
{
	private List<Animal> animalList;
	private Queue<Animal> animalQueue;
	private Map<String, Animal> animalMap;
	private Set<Animal> animalSets;

	public AnimalRepository()
	{	
		this.animalList = new ArrayList<Animal>();
		this.animalQueue = new LinkedList<Animal>();
		this.animalMap = new HashMap<String, Animal>();
		this.animalSets = new HashSet<Animal>();
	}

	public void add(Animal animalToAdd) 
	{
		this.animalList.add(animalToAdd);
		this.animalQueue.add(animalToAdd);
		this.animalMap.put(animalToAdd.animalID, animalToAdd);
		this.animalSets.add(animalToAdd);
		
		System.out.println("List size: " + this.animalList.size());
		System.out.println("Queue size: " + this.animalQueue.size());
		System.out.println("Map size: " + this.animalMap.size());
		System.out.println("Set size: " + this.animalSets.size());
		System.out.println("Class type: "+ animalToAdd.getClass().getName());
		System.out.println("--------------------------------------------------\n");
	}

	public Collection<Animal> getAllAnimals() 
	{
		return animalList;
	}
	
	public Collection<Animal> getUniqueAnimals()
	{
		return this.animalSets;
	}
	
	public Collection<Animal> getAllDogs()
	{	
		List<Animal> dogsList = new ArrayList<Animal>();
		for(Animal animalType: this.animalList)
		{
			System.out.println("Class type in list: "+ animalType.getClass().getName());
			if(animalType.getClass() == Dog.class)
			{
				System.out.println("Hell yeah is a dog: "+ animalType.animalName);
				dogsList.add(animalType);
			}
		}
		return dogsList;
	}
	
	public Animal getAnimalById(String animalId)
	{
		return this.animalMap.get(animalId);
	}
	
	public Collection<Animal> getDogByDogName (String dogName)
	{
		System.out.println("Get dog by dog's name.");
		
		List<Animal> dogsList = new ArrayList<Animal>();
		for(Animal animalType: this.animalList)
		{	
			if(animalType.getClass() == Dog.class && ((Dog)animalType).dogName.contains(dogName))
			{
				System.out.println("Hell yeah is a dog and its name is: "+ animalType.animalName);
				dogsList.add(animalType);
			}
		}
		return dogsList;
	}
	
	public Animal getOldestAnimalAndRemove()
	{	
		Animal firstAnimal = null;
		if(!this.animalQueue.isEmpty())
		{
			firstAnimal= this.animalQueue.peek();
			this.animalQueue.remove(firstAnimal);
		}
		return firstAnimal;
	}
}
