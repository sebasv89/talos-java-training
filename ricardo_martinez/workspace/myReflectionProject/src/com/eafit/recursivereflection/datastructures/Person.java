package com.eafit.recursivereflection.datastructures;
import com.eafit.recursivereflection.datastructures.Profession;
public class Person {
	
	private double name;
	private int id;
	private int nationality;
	private Profession pre = new Profession();
	
	public double getName() {
		return name;
	}
	public void setName(double name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNationality() {
		return nationality;
	}
	public void setNationality(int nationality) {
		this.nationality = nationality;
	}
	public Profession getPre() {
		return pre;
	}
	public void setPre(Profession pre) {
		this.pre = pre;
	}
}