package com.eafit.recursivereflection.datastructures;

public class Profession {
	
	private int years = 0;	
	private String name = "Doctor";
	private Branch br = new Branch();
	public int getYears() {
		return years;
	}
	public void setYears(int years) {
		this.years = years;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Branch getBr() {
		return br;
	}
	public void setBr(Branch br) {
		this.br = br;
	}
	
}
