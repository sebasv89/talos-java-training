package com.eafit.recursivereflection.datastructures;

public class Branch {
	private int qty;

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}
}
