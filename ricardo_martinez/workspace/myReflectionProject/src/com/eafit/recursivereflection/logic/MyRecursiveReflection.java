package com.eafit.recursivereflection.logic;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class MyRecursiveReflection {

	public static String getObjectInformation(Object obj, int level)
			throws IllegalArgumentException, IllegalAccessException {
		StringBuilder returnValue = new StringBuilder();

		String indentCalc = level == 0 ? "" : "%-" + (30 * level) + "s";

		Class<? extends Object> objectClass = obj.getClass();
		Field[] fields = objectClass.getDeclaredFields();
		String className = objectClass.getSimpleName();
		returnValue.append(String.format(indentCalc, new String()));
		returnValue.append(String.format("logging a %s object%n", className));
		for (Field f1 : fields) {
			// Allows to read a private field
			f1.setAccessible(true);

			// Obtain data from object
			Object fieldValue = f1.get(obj);
			String fieldName = f1.getName();
			String fieldType = f1.getType().getSimpleName();

			if (!f1.getType().isPrimitive()) {
				if (!MyRecursiveReflection.evalStopCondition(fieldType)) {
					String inner = MyRecursiveReflection.getObjectInformation(
							fieldValue, level + 1);
					returnValue.append(inner);
				}
			} else {
				returnValue.append(String.format(indentCalc, new String()));
				returnValue.append(String.format("%s-%s: Value = %s%n",
						fieldName, fieldType, fieldValue));
			}
			// Print FieldInfo

		}
		return returnValue.toString();
		// Method[] objectMethods = obj.getClass().getMethods();
	}

	public static boolean evalStopCondition(String evaluate) {
		ArrayList<String> stopers = new ArrayList<String>();
		stopers.add("Byte");
		stopers.add("String");
		return stopers.contains(evaluate);
	}

	public static String indent(String toIndent, int level) {
		String indent = "	";
		for (int c = 0; c < level; c++)
			indent += indent;
		return indent + toIndent;
	}

}
