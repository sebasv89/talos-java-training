package com.eafit.reflectionproject.logic;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public final class MyReflectionUtility {

	public static String getObjectInformation(Object obj)
			throws IllegalArgumentException, IllegalAccessException {
		StringBuilder returnValue = new StringBuilder();

		Class objectClass = obj.getClass();
		Field[] fields = objectClass.getDeclaredFields();
		String className = objectClass.getSimpleName();

		returnValue.append(String.format("logging a %s object%n", className));
		for (Field f1 : fields) {
			// Allows to read a private field
			f1.setAccessible(true);

			// Obtain data from object
			Object fieldValue = f1.get(obj);
			String fieldName = f1.getName();
			String fieldType = f1.getType().getSimpleName();

			// Print FieldInfo
			returnValue.append(String.format("%s-%s: Value = %s%n", fieldName,
					fieldType, fieldValue));

		}
		return returnValue.toString();
		// Method[] objectMethods = obj.getClass().getMethods();
	}
}
