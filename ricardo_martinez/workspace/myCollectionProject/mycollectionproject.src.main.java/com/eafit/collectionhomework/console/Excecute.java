package com.eafit.collectionhomework.console;

import java.util.*;

import com.eafit.collectionhomework.logic.DataAccessController;
import com.eafit.collectionhomework.model.*;

public class Excecute {

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DataAccessController da = new DataAccessController();
		for (int c = 0; c < 5; c++) {
			Cat cat = new Cat();
			cat.setId(String.format("Animal%s", 1));
			da.add(cat);
		}
		for (int c = 0; c < 3; c++) {
			Dog dog = new Dog();
			dog.setId(String.format("Animal%s", 1));
			dog.setDogName(String.format("Dog%s", c));
			da.add(dog);
		}
		ArrayList<Animal> allAnimals = da.getAllAnimals();
		ArrayList<Dog> allDogs = da.getAllDogs();
		Dog dog = da.getDogByDogName("Dog2");
		Animal oldestAnimal = da.getOldestAnimalAndRemove();
		Animal animalById= da.getAnimalById("Animal1");
		HashSet<Animal> unique = da.getUniqueAnimals();
		
	}

}
