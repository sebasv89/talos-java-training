package com.eafit.collectionhomework.logic;

import java.util.*;

import com.eafit.collectionhomework.model.*;

public class DataAccessController {

	private ArrayList<Animal> animalsList = new ArrayList<Animal>();
	private ArrayList<Dog> dogsList = new ArrayList<Dog>();
	private ArrayList<Cat> catsList = new ArrayList<Cat>();
	private HashSet<Animal> animalsSet = new HashSet<Animal>();
	private HashMap<String, Dog> dogsMap = new HashMap<String, Dog>();
	private HashMap<String, Animal> animalsMap = new HashMap<String, Animal>();
	private LinkedList<Animal> animalsQueue = new LinkedList<Animal>();

	public ArrayList<Animal> getAllAnimals() {
		return animalsList;
	}

	public ArrayList<Dog> getAllDogs() {
		return dogsList;
	}

	public HashSet<Animal> getUniqueAnimals() {
		return animalsSet;
	}

	public Animal getOldestAnimalAndRemove() {
		return animalsQueue.poll();
	}

	public Dog getDogByDogName(String dogName) {
		return dogsMap.get(dogName);
	}
	
	public Animal getAnimalById(String animalId) {
		return animalsMap.get(animalId);
	}

	public void add(Animal animal) {
		if (animal instanceof Dog) {
			Dog dog = (Dog) animal;
			this.addToDogsList(dog);
			this.addToDogsMap(dog);
		}
		this.addToAminalsMap(animal);
		this.addToAminalsList(animal);
		this.addToAminalsQueue(animal);
		this.addToAminalsSet(animal);
	}
	
	public void addToDogsList(Dog dog){
		dogsList.add(dog);
	}
	
	public void addToDogsMap(Dog dog)
	{
		dogsMap.put(dog.getDogName(), dog);
	}
	
	public void addToAminalsList(Animal animal)
	{
		animalsList.add(animal);
	}
	
	public void addToAminalsQueue(Animal animal)
	{
		animalsQueue.add(animal);
	}
	
	public void addToAminalsSet(Animal animal)
	{
		animalsSet.add(animal);		
	}
	
	public void addToAminalsMap(Animal animal)
	{
		animalsMap.put(animal.getId(), animal);
	}

}
