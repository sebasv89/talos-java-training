package com.eafit.collectionhomework.model;

public abstract class Mammal extends Animal{

	public abstract void doAMammerThing();

}
