package com.eafit.collectionhomework.model;

public class Cat extends Animal {
	
	

	@Override
	public void live() {
		// TODO Auto-generated method stub

	}
	
	@Override
	public int hashCode() {
		int result = this.getId().hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {		
		Animal otherCat = (Animal)obj;
		if (!this.getId().equals(otherCat.getId()))
			return false;
		return true;
	}

	

}
