package com.eafit.collectionhomework.model;

public class Dog extends Mammal {

	private String dogName;

	@Override
	public void doAMammerThing() {
		System.out.println("Executing Do a mammer thing!!");

	}

	@Override
	public void live() {
		System.out.println("Executing Live!");

	}

	public void anotherDogMethod() {
		System.out.println("Executing another dog method!");
	}

	public int add(Integer a, Integer b) {
		return a + b;
	}

	public String getDogName() {
		return dogName;
	}

	public void setDogName(String dogName) {
		this.dogName = dogName;
	}

	@Override
	public int hashCode() {
		int result = this.getId().hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		Animal otherDog = (Animal)obj;
		if (!this.getId().equals(otherDog.getId()))
			return false;
		return true;
	}

}
