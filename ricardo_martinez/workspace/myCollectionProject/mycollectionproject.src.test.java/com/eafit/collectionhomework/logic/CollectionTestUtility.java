package com.eafit.collectionhomework.logic;

import java.util.ArrayList;
import java.util.Collection;

import com.eafit.collectionhomework.model.Animal;

public class CollectionTestUtility {

	public static <T> boolean AssertEquals(Collection<T> codOne,
			Collection<T> codTwo) {
		if (codOne == null || codTwo == null)
			return false;
		if (codOne.size() != codTwo.size())
			return false;
		for(T a : codOne)
		{
			if(!codTwo.contains(a))
				return false;
		}
		return true;
	}

}
