package com.eafit.collectionhomework.logic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Observer;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import com.eafit.collectionhomework.logic.*;
import com.eafit.collectionhomework.model.Animal;
import com.eafit.collectionhomework.model.Cat;
import com.eafit.collectionhomework.model.Dog;

@RunWith(MockitoJUnitRunner.class)
public class DataAccessControllerTest {

	DataAccessController da;

	ArrayList<Animal> listOfAnimalsToValidate = new ArrayList<Animal>();
	private ArrayList<Dog> dogsListToValidate = new ArrayList<Dog>();
	private ArrayList<Cat> catsListToValidate = new ArrayList<Cat>();
	private HashSet<Animal> animalsSetToValidate = new HashSet<Animal>();
	Dog dogToValiate;
	Animal animalToValidate;
	Animal firstAnimalToValidate;

	@Mock
	public Dog dog;

	@Mock
	public Animal animal;

	@Before
	public void Init() {
		da = new DataAccessController();
		Dog dog1 = new Dog();
		dog1.setDogName("Rambo");
		dog1.setId("123ABC");
		Dog dog2 = new Dog();
		dog2.setDogName("Trosky");
		dog2.setId("123ABCD");
		Dog dog3 = new Dog();
		dog3.setDogName("Malu");
		dog3.setId("123A");
		Cat cat1 = new Cat();
		cat1.setId("666321");
		Cat cat2 = new Cat();
		cat2.setId("ABC321");
		
		dogToValiate = dog2;
		animalToValidate = cat1;
		firstAnimalToValidate = dog1;

		// Adds the animals to the Collection Controller
		da.add(dog1);
		da.add(dog2);
		da.add(dog3);
		da.add(cat1);
		da.add(cat2);

		// Adds the animals to the list used to validate the method excecution
		listOfAnimalsToValidate.add(dog1);
		listOfAnimalsToValidate.add(dog2);
		listOfAnimalsToValidate.add(dog3);
		listOfAnimalsToValidate.add(cat1);
		listOfAnimalsToValidate.add(cat2);

		// Adds the dogs to the list used to validate the method excecution
		dogsListToValidate.add(dog1);
		dogsListToValidate.add(dog2);
		dogsListToValidate.add(dog3);

		// Adds the animals to the set used to validate the method excecution
		animalsSetToValidate.add(dog1);
		animalsSetToValidate.add(dog2);
		animalsSetToValidate.add(dog3);
		animalsSetToValidate.add(cat1);
		animalsSetToValidate.add(cat2);

	}

	
	
	@Test
	public void testGetUniqueAnimals()
	{
		HashSet<Animal> uniqueAnimals = da.getUniqueAnimals();
		boolean isEqual = CollectionTestUtility.AssertEquals(uniqueAnimals, listOfAnimalsToValidate);
		Assert.assertTrue(isEqual);
	}
	
	@Test
	public void testGetAllAnimals()
	{
		ArrayList<Animal> allAnimals = da.getAllAnimals();
		boolean isEqual = CollectionTestUtility.AssertEquals(allAnimals, listOfAnimalsToValidate);
		Assert.assertTrue(isEqual);
	}
	
	@Test
	public void testGetAllDogs()
	{
		ArrayList<Dog> allDogs = da.getAllDogs();
		boolean isEqual = CollectionTestUtility.AssertEquals(allDogs, dogsListToValidate);
		Assert.assertTrue(isEqual);
	}
	
	@Test
	public void testGetAnimalById()
	{		
		Animal result = da.getAnimalById("666321");
		Assert.assertEquals(result, animalToValidate);
	}
	
	@Test
	public void testGetDogByDogName()
	{		
		Dog result = da.getDogByDogName("Trosky");
		Assert.assertEquals(result, dogToValiate);
	}
	
	@Test
	public void testGetOldestAnimalAndRemove()
	{		
		Animal result = da.getOldestAnimalAndRemove();
		Assert.assertEquals(result, firstAnimalToValidate);
	}
	
	@Test
	public void testAddDog() {
		da.add(dog);
		Mockito.verify(dog).getDogName();
	}

}
