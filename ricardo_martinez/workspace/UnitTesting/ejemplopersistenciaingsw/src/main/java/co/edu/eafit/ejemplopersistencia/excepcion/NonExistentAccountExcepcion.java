/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package co.edu.eafit.ejemplopersistencia.excepcion;

/**
 *
 * @author raquel
 */
public class NonExistentAccountExcepcion extends Exception{

    public NonExistentAccountExcepcion(){
        super();
    }

    public NonExistentAccountExcepcion(String string) {
        super(string);
    }
}
