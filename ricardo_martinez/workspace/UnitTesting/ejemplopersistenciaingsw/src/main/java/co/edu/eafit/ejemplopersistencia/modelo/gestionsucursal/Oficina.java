package co.edu.eafit.ejemplopersistencia.modelo.gestionsucursal;



public class Oficina extends PuntosAtencion
{
    //Atributos
    private int idOficina;
    private String nombre;
    
    public Oficina (int idOficina, String nombre)
    {
    	this.idOficina = idOficina;
    	this.nombre = nombre;
    }
    
    public int getId ()
    {
    	return idOficina;
    }
}
