package co.edu.eafit.ejemplopersistencia.control;

import static org.mockito.Mockito.*;

import java.sql.SQLException;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.sun.org.apache.bcel.internal.generic.ACONST_NULL;

import co.edu.eafit.ejemplopersistencia.dao.*;
import co.edu.eafit.ejemplopersistencia.excepcion.*;
import co.edu.eafit.ejemplopersistencia.modelo.gestioncliente.Cliente;
import co.edu.eafit.ejemplopersistencia.modelo.gestionmvtosycuentas.Cuenta;

@RunWith(MockitoJUnitRunner.class)
public class TransferirFondosControllerTest {

	private static final int CLIENT_ID = 1;
	private static final int ORIGINACCOUNT_ID = 2;
	private static final int DESTINATIONACCOUNT_ID = 3;

	TransferirFondosController controller = new TransferirFondosController();

	@Mock
	private DaoFactory daoFactory;

	@Mock
	private DaoInterface daoClient;

	@Mock
	private DaoInterface daoCuenta;

	@Mock
	private Cliente client;

	@Mock
	private Cuenta originAccount;

	@Mock
	private Cuenta destinationAccount;

	@Before
	public void init() throws SQLException {
		when(daoFactory.getDaoCliente()).thenReturn(daoClient);
		when(daoFactory.getDaoCuenta()).thenReturn(daoCuenta);
		controller.setFactoriaDao(daoFactory);
	}

	@Test(expected = NonExistentClientException.class)
	public void testTransferWithNonExistentClient() throws Exception {
		when(daoClient.find(CLIENT_ID)).thenReturn(null);

		controller.transfer(CLIENT_ID, 4, 5, 3212);
	}

	@Test(expected = NonExistentAccountExcepcion.class)
	public void testTransferWithNonExistentOriginAccountExcepcion()
			throws Exception {
		when(daoClient.find(CLIENT_ID)).thenReturn(client);
		when(daoCuenta.find(ORIGINACCOUNT_ID)).thenReturn(null);

		controller.transfer(CLIENT_ID, ORIGINACCOUNT_ID, 5, 3212);
	}

	@Test(expected = NonExistentAccountExcepcion.class)
	public void testTransferWithNonExistentAccountExcepcion() throws Exception {
		when(daoClient.find(CLIENT_ID)).thenReturn(client);
		when(daoCuenta.find(ORIGINACCOUNT_ID)).thenReturn(originAccount);
		when(daoCuenta.find(DESTINATIONACCOUNT_ID)).thenReturn(null);

		controller.transfer(CLIENT_ID, ORIGINACCOUNT_ID, DESTINATIONACCOUNT_ID,
				3212);
	}

	@Test(expected = InvalidClientExcepcion.class)
	public void testTransferWithInvalidClientExcepcion() throws Exception {
		when(daoClient.find(CLIENT_ID)).thenReturn(client);
		when(daoCuenta.find(ORIGINACCOUNT_ID)).thenReturn(originAccount);
		when(daoCuenta.find(DESTINATIONACCOUNT_ID)).thenReturn(
				destinationAccount);
		when(originAccount.getCliente()).thenReturn(client);
		when(client.getId()).thenReturn(ORIGINACCOUNT_ID + 1);

		controller.transfer(CLIENT_ID, ORIGINACCOUNT_ID, DESTINATIONACCOUNT_ID,
				3212);
	}
	
	@Test(expected = InvalidClientExcepcion.class)
	public void testTransfer() throws Exception {
		when(daoClient.find(CLIENT_ID)).thenReturn(client);
		when(daoCuenta.find(ORIGINACCOUNT_ID)).thenReturn(originAccount);
		when(daoCuenta.find(DESTINATIONACCOUNT_ID)).thenReturn(
				destinationAccount);
		when(originAccount.getCliente()).thenReturn(client);
		when(client.getId()).thenReturn(ORIGINACCOUNT_ID);

		String returnValue = controller.transfer(CLIENT_ID, ORIGINACCOUNT_ID, DESTINATIONACCOUNT_ID,
				3212);
		Assert.assertEquals(returnValue, "Transferencia Exitosa");
	}

}
