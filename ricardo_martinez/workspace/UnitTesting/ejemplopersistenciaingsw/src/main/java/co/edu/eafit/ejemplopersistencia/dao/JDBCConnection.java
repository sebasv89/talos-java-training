/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package co.edu.eafit.ejemplopersistencia.dao;

import java.sql.DriverManager;

import com.mysql.jdbc.Connection;

/**
 *
 * @author raquel
 */
public class JDBCConnection {

    	//private Session session = null;
	private static Connection connection;

	/**
	 *
	 *
	 */
        public  static Connection getConexion() throws Exception{
            if (connection == null)
                    return getJDBCConnection();
                else
                    return connection;
        }
       	private static   Connection getJDBCConnection() throws Exception{
		//session  = ConfiguracionHibernate.getInstance().getSession();
		
			// Register the JDBC driver for MySQL.
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:3306/db_bancos";
			connection = (Connection) DriverManager.getConnection(url, "root", "root");
		return connection;
	}


	/**
	 *
	 *
	 */
	public static void cerrarConexion(){
		try{
                            connection.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
