package co.edu.eafit.ejemplopersistencia.modelo.gestionsucursal;



public class Cajero extends PuntosAtencion
{
    //Atributos
    private int dineroDisponible;
    private int idCajero;
    
    //Constructor
    public Cajero (int idCajero, int dinero)
    {
    	this.idCajero = idCajero;
    	this.dineroDisponible = dinero;
    }
    
    public int getId ()
    {
    	return idCajero;
    }
    

}
