/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package co.edu.eafit.ejemplopersistencia.dao;

/**
 *
 * @author raquel
 */
public interface Entidad {
// Para verificar si los objetos corresponden al mismo registro de base de datos (identidad de clave primaria)
	public boolean esIgual( Entidad entidad );
// para obtener el valor actual de la entidad
        public String toString();
 }