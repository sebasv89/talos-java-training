package co.edu.eafit.ejemplopersistencia.modelo.gestionmvtosycuentas;

import java.util.Date;

import java.util.Calendar;

import co.edu.eafit.ejemplopersistencia.excepcion.SaldoInsuficienteExcepcion;
import co.edu.eafit.ejemplopersistencia.modelo.gestioncliente.Cliente;


public class Corriente extends Cuenta
{
	//Atributos
    private int topeSobrejiro;
    
    public Corriente() {
        //
    }

    public Corriente(String intfechaApertura, Cliente cliente, int saldo, int topeSobrejiro, int idCuenta)
    {
		super(intfechaApertura, cliente, saldo, idCuenta);
		this.topeSobrejiro = topeSobrejiro;
	}

  
    
    /*
     * M�todo para retirar la cantidad x de la cuenta
     */
    public void retirar (int x) throws SaldoInsuficienteExcepcion
    {
         System.out.println("entra a retirar corriente ");
    	saldo = saldo - x;
    	if (saldo < 0)
    	{
    	   if (saldo + topeSobrejiro < 0)
    		{
	    		saldo = saldo + x;
	    		throw new SaldoInsuficienteExcepcion ("Saldo Insuficiente");
    		}
        }
           addMvtoCuenta(MvtosCuenta.SALIDA, x);
               
         }
   

    public int getTopeSobrejiro() {
        return topeSobrejiro;
    }

    public void setTopeSobrejiro(int topeSobrejiro) {
        this.topeSobrejiro = topeSobrejiro;
    }

   
}
