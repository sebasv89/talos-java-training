/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package co.edu.eafit.ejemplopersistencia.dao;

/**
 * 
 * @author raquel
 */
public class DaoFactory {
	// la clase puede ser instanciada a partir de un archivo de propiedades
	public DaoInterface getDaoCliente() {
		return new ClienteDAO();
	}

	public DaoInterface getDaoCuenta() {
		return new CuentaDAO();
	}

	public DaoInterface getDaoSucursal() {
		return new SucursalDAO();
	}
}
