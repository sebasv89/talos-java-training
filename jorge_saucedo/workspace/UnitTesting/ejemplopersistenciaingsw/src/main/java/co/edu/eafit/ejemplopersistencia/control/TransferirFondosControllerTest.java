package co.edu.eafit.ejemplopersistencia.control;

import static org.mockito.Mockito.*;

import java.sql.SQLException;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import co.edu.eafit.ejemplopersistencia.dao.DaoFactory;
import co.edu.eafit.ejemplopersistencia.dao.DaoInterface;
import co.edu.eafit.ejemplopersistencia.excepcion.*;
import co.edu.eafit.ejemplopersistencia.modelo.gestioncliente.Cliente;
import co.edu.eafit.ejemplopersistencia.modelo.gestionmvtosycuentas.Cuenta;

@RunWith(MockitoJUnitRunner.class)
public class TransferirFondosControllerTest {

	private static final int CLIENT_ID = 1;
	private static final int SOURCE_ACCOUNT = 4;
	private static final int DESTINATION_ACCOUNT = 5;

	TransferirFondosController controller = new TransferirFondosController();

	@Mock
	private DaoFactory daoFactory;

	@Mock
	private DaoInterface daoClient;

	@Mock
	private DaoInterface daoCuenta;
	
	@Mock
	private Cliente client;
	
	@Mock
	private Cuenta sourceAccount, destinationAccount;

	@Before
	public void init() throws SQLException {
		when(daoFactory.getDaoCliente()).thenReturn(daoClient);
		when(daoFactory.getDaoCuenta()).thenReturn(daoCuenta);
		controller.setFactoriaDao(daoFactory);
	}
	
	@Test(expected = NonExistentClientException.class)
	public void testTransferWithNonExistentClient() throws Exception {
		when(daoClient.find(CLIENT_ID)).thenReturn(null);
		controller.transfer(CLIENT_ID, SOURCE_ACCOUNT, DESTINATION_ACCOUNT, 3212);
	}

	@Test(expected = CuentaInexistenteExcepcion.class)
	public void testTransferWithNonExistentSourceAccount() throws Exception{
		when(daoClient.find(CLIENT_ID)).thenReturn(client);
		when(daoCuenta.find(SOURCE_ACCOUNT)).thenReturn(null);
		controller.transfer(CLIENT_ID, SOURCE_ACCOUNT, DESTINATION_ACCOUNT, 3212);
	}
	
	@Test(expected = CuentaInexistenteExcepcion.class)
	public void testTransferWithNonExistentDestinationAccount() throws Exception{
		when(daoClient.find(CLIENT_ID)).thenReturn(client);
		when(daoCuenta.find(SOURCE_ACCOUNT)).thenReturn(sourceAccount);
		when(daoCuenta.find(DESTINATION_ACCOUNT)).thenReturn(null);
		controller.transfer(CLIENT_ID, SOURCE_ACCOUNT, DESTINATION_ACCOUNT, 3212);
	}
	
	@Test(expected = ClienteInvalidoExcepcion.class)
	public void testTransferWithNonValidClient() throws Exception{
		when(daoClient.find(CLIENT_ID)).thenReturn(client);
		when(daoCuenta.find(SOURCE_ACCOUNT)).thenReturn(sourceAccount);
		when(daoCuenta.find(DESTINATION_ACCOUNT)).thenReturn(destinationAccount);
		when(sourceAccount.getCliente()).thenReturn(client);
		when(client.getId()).thenReturn(-1);
		controller.transfer(CLIENT_ID, SOURCE_ACCOUNT, DESTINATION_ACCOUNT, 3212);
	}
	
	@Test
	public void testTransfer() throws Exception{
		when(daoClient.find(CLIENT_ID)).thenReturn(client);
		when(daoCuenta.find(SOURCE_ACCOUNT)).thenReturn(sourceAccount);
		when(daoCuenta.find(DESTINATION_ACCOUNT)).thenReturn(destinationAccount);
		when(sourceAccount.getCliente()).thenReturn(client);
		when(client.getId()).thenReturn(CLIENT_ID);
		String transferResult = controller.transfer(CLIENT_ID, SOURCE_ACCOUNT, DESTINATION_ACCOUNT, 3212);
		Assert.assertEquals("Transferencia Exitosa", transferResult);
	}

}
