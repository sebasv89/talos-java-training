/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package co.edu.eafit.ejemplopersistencia.excepcion;

/**
 *
 * @author eafit
 */
public class NonExistentClientException extends Exception {
  public NonExistentClientException(String string) {
        super(string);
    }
}
