package co.edu.eafit.ejemplopersistencia.modelo.gestioncliente;

import co.edu.eafit.ejemplopersistencia.dao.Entidad;

public class Cliente implements Entidad
{
    //Atributos
    private int idCliente;
    private String nombre;
    private String apellido;
    //Constructor
    
    public Cliente (int id, String nombre, String apellido)
    {
    	this.idCliente = id;
    	this.nombre = nombre;
    	this.apellido = apellido;
    }

    public Cliente() {
       
    }
    
    //Gets and Sets
    
    public int getId()
    {
    	return idCliente;
    }
    public String getNombre ()
    {
    	return nombre;
    }
    public String getApellido ()
    {
    	return apellido;
    }
    public void setIdCliente(int id){
        idCliente = id;
    }
    public void setNombre(String n){
        nombre = n;
    }
     public void setApellido(String a){
        apellido = a;
    }
    public boolean esIgual(Entidad entidad) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
