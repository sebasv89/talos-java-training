package co.edu.eafit.ejemplopersistencia.modelo.gestionmvtosycuentas;

import java.io.*;

import java.util.Date;

import co.edu.eafit.ejemplopersistencia.modelo.gestionsucursal.*;

public class MvtosCuenta 
{
	//Constantes
	public static final int ENTRADA = 1;
	public static final int SALIDA = 2;

        //Atributos
        private int tipoMvto;
        private int valorMvto;
        private Date fechaMvto;
        private boolean nuevo = false;

	
	//Constructor
	public MvtosCuenta (int tipo, int valor, Date fecha)
	{
		tipoMvto = tipo;
                valorMvto = valor;
                fechaMvto = fecha;
	}
        public boolean esNuevo(){
            return nuevo;
        }
        public void setNuevo(boolean flag){
            nuevo = flag;
        }
        public int getTipoMvto(){
            return tipoMvto;
        }
        public int getValorMvto(){
            return valorMvto;
        }
        public Date getFechaMvto(){
            return fechaMvto;
        }
}