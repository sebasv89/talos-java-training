package co.edu.eafit.capacitacionjava.reflection.clases;

public class Empresa {

	private String nombre;
	private String direccion;
	private Empleado empleado;
	private int telefono;
	private double nit;


	public Empresa(String nombre, String direccion, int telefono, double nit, Empleado empleado) {
		super();
		this.nombre = nombre;
		this.direccion = direccion;
		this.telefono = telefono;
		this.nit = nit;
		this.empleado = empleado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	public double getNit() {
		return nit;
	}

	public void setNit(double nit) {
		this.nit = nit;
	}

	public Empleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	
}
