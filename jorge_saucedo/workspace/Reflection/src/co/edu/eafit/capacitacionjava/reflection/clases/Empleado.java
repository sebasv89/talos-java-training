package co.edu.eafit.capacitacionjava.reflection.clases;

public class Empleado {
	private float numeroEmpleado;
	private Persona persona;

	public Empleado(float numeroEmpleado, Persona persona) {
		this.numeroEmpleado = numeroEmpleado;
		this.persona = persona;
	}

	public float getNumeroEmpleado() {
		return numeroEmpleado;
	}

	public void setNumeroEmpleado(float numeroEmpleado) {
		this.numeroEmpleado = numeroEmpleado;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}
}
