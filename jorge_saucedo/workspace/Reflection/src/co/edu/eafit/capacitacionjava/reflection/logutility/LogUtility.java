package co.edu.eafit.capacitacionjava.reflection.logutility;

import java.lang.reflect.*;

public class LogUtility {

	private static final int ESPACIO_EN_BLANCO = 20;

	private static final String[] TIPOS_DATO = { "int", "String", "double",
			"char", "float" };

	public static String getObjectLog(Object objeto, StringBuilder respuesta,
			int nivel) {
		Class myObjectClass = objeto.getClass();
		String className = myObjectClass.getSimpleName();
		Field[] atributos = myObjectClass.getDeclaredFields();
		if (atributos.length > 0) {
			if (nivel > 0) {
				respuesta.append(String.format(LogUtility.getSangria(nivel),
						new String()));
			}
			respuesta
					.append(String.format("logging a %s object %n", className));

			for (Field atributo : atributos) {
				try {
					atributo.setAccessible(true);
					String nombreAtributo = atributo.getName();
					String nombreTipo = atributo.getType().getSimpleName();
					Object valorAtributo = atributo.get(objeto);
					if (nivel > 0) {
						respuesta.append(String.format(
								LogUtility.getSangria(nivel), new String()));
					}
					respuesta.append(String.format("%s-%s: Value = %s %n",
							nombreAtributo, nombreTipo,
							valorAtributo.toString()));

					if (LogUtility.isObject(nombreTipo)) {
						nivel++;
						LogUtility.getObjectLog(atributo.get(objeto),
								respuesta, nivel);
					}

					/*
					 * if (!atributo.getType().isPrimitive() &&
					 * !nombreTipo.equals("String")) { nivel++;
					 * LogUtility.getObjectLog(atributo.get(objeto), respuesta,
					 * nivel); }
					 */

				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
		return respuesta.toString();
	}

	private static String getSangria(int nivel) {
		int sangria = nivel * ESPACIO_EN_BLANCO;
		return "%" + sangria + "s";
	}

	private static boolean isObject(String tipo) {
		for (String tipoDato : TIPOS_DATO) {
			if (tipoDato.equals(tipo)) {
				return false;
			}
		}
		return true;
	}

}
