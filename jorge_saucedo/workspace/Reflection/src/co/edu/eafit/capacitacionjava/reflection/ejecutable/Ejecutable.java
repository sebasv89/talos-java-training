package co.edu.eafit.capacitacionjava.reflection.ejecutable;

import co.edu.eafit.capacitacionjava.reflection.clases.*;
import co.edu.eafit.capacitacionjava.reflection.logutility.*;

public class Ejecutable {

	public static void main(String[] args) {
		Persona persona = new Persona("Jorge Enrique", "Saucedo Jaramillo",
				94538978);
		Empleado empleado = new Empleado(123, persona);
		Empresa empresa = new Empresa("SonArdev Ltda.", "Calle 11A #43D-46", 3123305,
				900237611, empleado);
		StringBuilder respuesta = new StringBuilder();
		int nivel = 0;
		String salida = LogUtility.getObjectLog(empresa, respuesta, nivel);
		System.out.println(salida);
	}
}
