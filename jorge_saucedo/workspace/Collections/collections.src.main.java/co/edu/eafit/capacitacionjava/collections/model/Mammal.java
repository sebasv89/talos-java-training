package co.edu.eafit.capacitacionjava.collections.model;

public abstract class Mammal extends Animal {

	public abstract void doAMammerThing();

}
