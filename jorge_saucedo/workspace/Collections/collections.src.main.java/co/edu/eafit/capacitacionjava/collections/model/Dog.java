package co.edu.eafit.capacitacionjava.collections.model;

public class Dog extends Mammal {

	private String dogName;

	@Override
	public void doAMammerThing() {
		System.out.println("Executing Do a mammer thing!!");

	}

	@Override
	public void live() {
		System.out.println("Executing Live!");

	}

	public void anotherDogMethod() {
		System.out.println("Executing another dog method!");
	}

	public int add(Integer a, Integer b) {
		return a + b;
	}

	public String getDogName() {
		return dogName;
	}

	public void setDogName(String dogName) {
		this.dogName = dogName;
	}
	
	public boolean equals(Object o) {
		if (o instanceof Animal) {
			Animal animal = (Animal) o;
			if (animal.getId().equals(this.getId())) {
				return true;
			}
		}
		return false;
	}

	public int hashCode() {
		return this.getId().hashCode();
	}

}
