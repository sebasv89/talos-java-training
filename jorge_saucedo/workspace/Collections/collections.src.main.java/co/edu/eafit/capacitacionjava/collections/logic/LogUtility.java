package co.edu.eafit.capacitacionjava.collections.logic;

import java.lang.reflect.*;

public class LogUtility {

	public static String getObjectLog(Object objeto, StringBuilder respuesta) {
		Class myObjectClass = objeto.getClass();
		String className = myObjectClass.getSimpleName();
		Field[] atributos = myObjectClass.getDeclaredFields();
		if (atributos.length > 0) {
			respuesta
					.append(String.format("logging a %s object %n", className));

			for (Field atributo : atributos) {
				try {
					atributo.setAccessible(true);
					String nombreAtributo = atributo.getName();
					String nombreTipo = atributo.getType().getSimpleName();
					Object valorAtributo = atributo.get(objeto);
					respuesta.append(String.format("%s-%s: Value = %s %n",
							nombreAtributo, nombreTipo,
							valorAtributo.toString()));
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
		return respuesta.toString();
	}
}
