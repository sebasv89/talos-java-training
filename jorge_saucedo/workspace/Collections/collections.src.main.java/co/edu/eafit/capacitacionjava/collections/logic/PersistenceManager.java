package co.edu.eafit.capacitacionjava.collections.logic;

import java.util.*;

import co.edu.eafit.capacitacionjava.collections.model.*;

public class PersistenceManager {

	private HashMap<String, ArrayList<Animal>> mapAnimals;
	private Hashtable<String, Dog> tableNameDogs;
	private Hashtable<Integer, Animal> tableIdAnimals;
	private ArrayList<Animal> listAnimals;
	private HashSet<Animal> setAnimals;
	private ArrayDeque<Animal> queueAnimals;

	public PersistenceManager() {
		this.mapAnimals = new HashMap<String, ArrayList<Animal>>();
		this.listAnimals = new ArrayList<Animal>();
		this.setAnimals = new HashSet<Animal>();
		this.queueAnimals = new ArrayDeque<Animal>();
		this.tableIdAnimals = new Hashtable<Integer, Animal>();
		this.tableNameDogs = new Hashtable<String, Dog>();
	}

	public void add(Animal animal) {
		// adiciono animal al map
		this.addAnimalMap(animal);

		// adidiono animal al list
		this.addAnimalList(animal);

		// adiciono animal al set
		this.addAnimalSet(animal);

		// adiciono animal a la cola
		this.addAnimalQueue(animal);

		// adiciono animal al table con el ID
		this.addAnimalIdMap(animal);

		// adiciono perro al table con el nombre
		this.addDogNameMap(animal);
	}

	private void addAnimalMap(Animal animal) {
		Class myObjectClass = animal.getClass();
		String animalKind = myObjectClass.getSimpleName();

		if (this.mapAnimals.containsKey(animalKind)) {
			ArrayList<Animal> animals = this.mapAnimals.get(animalKind);
			animals.add(animal);
		} else {
			ArrayList<Animal> animals = new ArrayList<Animal>();
			animals.add(animal);
			this.mapAnimals.put(animalKind, animals);
		}
	}

	private void addAnimalList(Animal animal) {
		this.listAnimals.add(animal);
	}

	private void addAnimalSet(Animal animal) {
		this.setAnimals.add(animal);
	}

	private void addAnimalQueue(Animal animal) {
		this.queueAnimals.add(animal);
	}

	private void addDogNameMap(Animal animal) {
		if (animal instanceof Dog) {
			Dog perro = (Dog) animal;
			this.tableNameDogs.put(perro.getDogName(), perro);
		}
	}

	private void addAnimalIdMap(Animal animal) {
		this.tableIdAnimals.put(animal.getId(), animal);
	}

	public Dog[] getAllDogs() {
		ArrayList<Animal> perros = this.mapAnimals.get("Dog");
		Dog[] dogs = new Dog[perros.size()];
		dogs = perros.toArray(dogs);
		return dogs;
	}

	public Animal[] getAllAnimals() {
		Animal[] animals = new Animal[this.listAnimals.size()];
		return this.listAnimals.toArray(animals);
	}

	public Animal[] getUniqueAnimals(Animal [] animals) {
		animals = new Animal[this.setAnimals.size()];
		return this.setAnimals.toArray(animals);
	}

	public Animal getOldestAnimalAndRemove() {
		return this.queueAnimals.pop();
	}

	public Animal getAnimalById(Integer id) {
		return this.tableIdAnimals.get(id);
	}

	public Dog getDogByDogName(String nombre) {
		return this.tableNameDogs.get(nombre);
	}

	public void setMapAnimals(HashMap<String, ArrayList<Animal>> mapAnimals) {
		this.mapAnimals = mapAnimals;
	}

	public void setTableNameDogs(Hashtable<String, Dog> tableNameDogs) {
		this.tableNameDogs = tableNameDogs;
	}

	public void setTableIdAnimals(Hashtable<Integer, Animal> tableIdAnimals) {
		this.tableIdAnimals = tableIdAnimals;
	}

	public void setListAnimals(ArrayList<Animal> listAnimals) {
		this.listAnimals = listAnimals;
	}

	public void setSetAnimals(HashSet<Animal> setAnimals) {
		this.setAnimals = setAnimals;
	}

	public void setQueueAnimals(ArrayDeque<Animal> queueAnimals) {
		this.queueAnimals = queueAnimals;
	}

}
