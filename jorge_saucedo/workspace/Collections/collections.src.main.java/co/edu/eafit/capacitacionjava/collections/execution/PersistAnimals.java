package co.edu.eafit.capacitacionjava.collections.execution;

import co.edu.eafit.capacitacionjava.collections.logic.*;
import co.edu.eafit.capacitacionjava.collections.model.*;

public class PersistAnimals {

	public static void main(String[] args) {
		Dog perro = new Dog();
		perro.setDogName("Rufo");
		perro.setId(0);

		Dog perro2 = new Dog();
		perro2.setDogName("Lucas");
		perro2.setId(1);

		Dog perro3 = new Dog();
		perro3.setDogName("Trotsky");
		perro3.setId(2);

		Cat gato = new Cat();
		gato.setCatName("Dalila");
		gato.setId(10);

		Cat gato2 = new Cat();
		gato2.setCatName("Sanson");
		gato2.setId(11);

		Cat gato3 = new Cat();
		gato3.setCatName("Praga");
		gato3.setId(1);

		PersistenceManager p = new PersistenceManager();
		p.add(perro);
		p.add(perro2);
		p.add(perro3);
		p.add(gato);
		p.add(gato2);
		p.add(gato3);

		System.out.println("RESULTADO M�TODO getAllDogs()");
		Dog[] perros = p.getAllDogs();
		for (Dog pet : perros) {
			System.out.println(LogUtility
					.getObjectLog(pet, new StringBuilder()));
		}

		System.out.println("RESULTADO M�TODO getAllAnimals()");
		Animal[] animales = p.getAllAnimals();
		for (Animal animal : animales) {
			System.out.println(LogUtility.getObjectLog(animal,
					new StringBuilder()));
		}

		System.out.println("RESULTADO M�TODO getUniqueAnimals()");
		
		animales = p.getUniqueAnimals(animales);
		for (Animal animal : animales) {
			System.out.println(LogUtility.getObjectLog(animal,
					new StringBuilder()));
		}

		System.out.println("RESULTADO M�TODO getOldestAnimalAndRemove()");
		Animal animal = p.getOldestAnimalAndRemove();
		System.out
				.println(LogUtility.getObjectLog(animal, new StringBuilder()));

		System.out.println("RESULTADO M�TODO getAnimalById()");
		animal = p.getAnimalById(1);
		System.out
				.println(LogUtility.getObjectLog(animal, new StringBuilder()));

		System.out.println("RESULTADO M�TODO getDogByDogName()");
		Dog can = p.getDogByDogName("Lucas");
		System.out.println(LogUtility.getObjectLog(can, new StringBuilder()));
	}

}
