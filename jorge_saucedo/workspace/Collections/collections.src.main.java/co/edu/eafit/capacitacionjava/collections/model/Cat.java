package co.edu.eafit.capacitacionjava.collections.model;

public class Cat extends Animal {
	
	private String catName;

	@Override
	public void live() {
		// TODO Auto-generated method stub

	}

	public String getCatName() {
		return catName;
	}

	public void setCatName(String catName) {
		this.catName = catName;
	}

	public boolean equals(Object o) {
		if (o instanceof Animal) {
			Animal animal = (Animal) o;
			if (animal.getId().equals(this.getId())) {
				return true;
			}
		}
		return false;
	}

	public int hashCode() {
		return this.getId().hashCode();
	}

}
