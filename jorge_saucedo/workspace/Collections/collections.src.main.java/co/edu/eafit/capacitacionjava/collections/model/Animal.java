package co.edu.eafit.capacitacionjava.collections.model;

public abstract class Animal {

	private Integer id;

	public abstract void live();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public boolean equals(Object o) {
		if (o instanceof Animal) {
			Animal animal = (Animal) o;
			if (animal.id.equals(this.id)) {
				return true;
			}
		}
		return false;
	}

	public int hashCode() {
		return id.hashCode();
	}
}
