package co.edu.eafit.capacitacionjava.collections.logic;

import static org.mockito.Mockito.*;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import co.edu.eafit.capacitacionjava.collections.model.Animal;
import co.edu.eafit.capacitacionjava.collections.model.Dog;

@RunWith(MockitoJUnitRunner.class)
public class PersistenceManagerTest {

	private PersistenceManager persistenceManager = new PersistenceManager();
	private Dog dog = new Dog();

	@Mock
	private HashMap<String, ArrayList<Animal>> mapAnimals;

	@Mock
	private Hashtable<String, Dog> tableNameDogs;

	@Mock
	private Hashtable<Integer, Animal> tableIdAnimals;

	@Mock
	private ArrayList<Animal> listAnimals;

	@Mock
	private HashSet<Animal> setAnimals;

	@Mock
	private ArrayDeque<Animal> queueAnimals;

	@Before
	public void init() {
		dog.setDogName("Rufo");
		dog.setId(2);
		persistenceManager.setListAnimals(listAnimals);
		persistenceManager.setMapAnimals(mapAnimals);
		persistenceManager.setQueueAnimals(queueAnimals);
		persistenceManager.setSetAnimals(setAnimals);
		persistenceManager.setTableIdAnimals(tableIdAnimals);
		persistenceManager.setTableNameDogs(tableNameDogs);

	}

	@Test
	public void addTest() {
		ArrayList<Animal> dogs = new ArrayList<Animal>();
		when(mapAnimals.put("Dog", dogs)).thenReturn(dogs);
		when(tableNameDogs.put("Rufo", dog)).thenReturn(dog);
		when(tableIdAnimals.put(2, dog)).thenReturn(dog);
		when(listAnimals.add(dog)).thenReturn(true);
		when(setAnimals.add(dog)).thenReturn(true);
		when(queueAnimals.add(dog)).thenReturn(true);
		persistenceManager.add(dog);
		
		/*
		 * For this method I don't understand why the verify method of Mockito
		 * is expecting an empty array. The other verification still working but the
		 * one of mapAnimals doesn't work */
		Mockito.verify(mapAnimals, atLeastOnce()).put("Dog", dogs);
		Mockito.verify(tableNameDogs, atLeastOnce()).put("Rufo", dog);
		Mockito.verify(tableIdAnimals, atLeastOnce()).put(2, dog);
		Mockito.verify(listAnimals, atLeastOnce()).add(dog);
		Mockito.verify(setAnimals, atLeastOnce()).add(dog);
		Mockito.verify(queueAnimals, atLeastOnce()).add(dog);
	}

	@Test
	public void getAllDogsTest() {
		ArrayList<Animal> dogs = new ArrayList<Animal>();
		dogs.add(dog);
		when(mapAnimals.get("Dog")).thenReturn(dogs);
		Dog[] actualDogs = persistenceManager.getAllDogs();
		Dog[] expectedeDogs = new Dog[dogs.size()];
		dogs.toArray(expectedeDogs);
		Assert.assertEquals(true,
				this.compareAnimals(expectedeDogs, actualDogs));
	}

	@Test
	public void getAllAnimalsTest() {
		/*
		 * In this method, i understand that the Mock is receiving an array
		 * of animals, but this array is different than the one that is created
		 * in the method inside the persistenceManager class, that's why the mock
		 * returns a null values.
		 * I think the way to solve this issue is to change the method "getAllAnimals()".
		 * If I pass as a parameter the array, then the mock will return the expected value,
		 * but I want to know if it is the only way to do it, or if it's another way
		 * to validate this method.
		 * I've tried to do it in the next method, but I still having a null reference.
		 * 
		 */
		ArrayList<Animal> arrayListAnimals = new ArrayList<Animal>();
		arrayListAnimals.add(dog);
		Animal[] animals = new Animal[arrayListAnimals.size()];
		arrayListAnimals.toArray(animals);

		when(listAnimals.toArray(animals)).thenReturn(animals);
		Animal[] actualAnimals = persistenceManager.getAllAnimals();
		Assert.assertEquals(true, this.compareAnimals(animals, actualAnimals));

	}

	@Test
	public void getUniqueAnimalsTest() {
		HashSet<Animal> setExpectedAnimals = new HashSet<Animal>();
		setExpectedAnimals.add(dog);
		Animal [] animals = new Animal[setExpectedAnimals.size()];
		Animal [] expectedUniqueAnimals = setExpectedAnimals.toArray(animals);
		when(setAnimals.toArray(animals)).thenReturn(expectedUniqueAnimals);
		Animal [] actualUniqueAnimals = persistenceManager.getUniqueAnimals(animals);
		Assert.assertEquals(true, this.compareAnimals(expectedUniqueAnimals, actualUniqueAnimals));
		
	}
	
	@Test
	public void getOldestAnimalAndRemoveTest() {
		when(queueAnimals.pop()).thenReturn(dog);
		Animal actualAnimal = persistenceManager.getOldestAnimalAndRemove();
		Assert.assertEquals(dog, actualAnimal);
	}

	@Test
	public void getAnimalByIdTest() {
		when(tableIdAnimals.get(2)).thenReturn(dog);
		Animal actualAnimal = persistenceManager.getAnimalById(2);
		Assert.assertEquals(dog, actualAnimal);
	}

	@Test
	public void getDogByDogNameTest() {
		when(tableNameDogs.get("Rufo")).thenReturn(dog);
		Dog actualDog = persistenceManager.getDogByDogName("Rufo");
		Assert.assertEquals(dog, actualDog);
	}

	private boolean compareAnimals(Animal[] expectedAnimals,
			Animal[] actualAnimals) {
		if (expectedAnimals != null && actualAnimals != null) {
			if (expectedAnimals.length == actualAnimals.length) {
				for (int i = 0; i < expectedAnimals.length; i++) {
					if (!expectedAnimals[i].equals(actualAnimals[i])) {
						return false;
					}
				}
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}
