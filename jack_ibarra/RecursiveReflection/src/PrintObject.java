import java.lang.reflect.Field;
public class PrintObject {
	static String[] noIter = new String[]{"int","String"};
	public static void extractValues(Object instance,int ident) throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException{
		System.out.printf("%"+ident+"s"+instance.getClass().getName()+" Object  \n","");
		for (Field field : instance.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			if (isIteratorClass(field.getType().getSimpleName())) {
				System.out.printf("%"+ident+"s"+field.getName()+"-"+field.getType().getSimpleName()+": Value = "+field.get(instance).toString()+"\n","");
			}else{
				extractValues(field.get(instance), ident+2);
			}
		}
	}
	public static boolean isIteratorClass(String simpleName) {
		return (simpleName.equals(noIter[0]) || simpleName.equals(noIter[1]));
	}
}
