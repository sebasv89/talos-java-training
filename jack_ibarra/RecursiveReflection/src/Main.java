
public class Main {
	public static void main(String[] args) throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
		Car car=new Car(1998,"Black","BMW");
		Person jack = new Person(1,"Jack", "Ibarra",car);
		PrintObject.extractValues(jack,1);
	}
}
