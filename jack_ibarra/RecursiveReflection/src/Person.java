
public class Person {
	private int id;
	private String firstName;
	private String lastName;
	private Car car;
	public Person(int id, String firstName, String lastName, Car car) {
		this.lastName = lastName;
		this.firstName = firstName;
		this.id = id;
		this.car = car;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}
		
	
	
}
