public class Car {
	private int model;
	private String color;
	private String brand;
	
	public Car(int model,String color, String brand) {
		this.model = model;
		this.color = color;
		this.brand = brand;
	}

	public int getModel() {
		return model;
	}

	public void setModel(int model) {
		this.model = model;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}
	

	
}
