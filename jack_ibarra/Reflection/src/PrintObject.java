import java.lang.reflect.Field;
public class PrintObject {
	public static void extractValues(Object instance) throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException{
		System.out.println("logging a "+instance.getClass().getName()+" object");
		for (Field field : instance.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			System.out.println(field.getName()+"-"+field.getType().getSimpleName()+": Value = "+field.get(instance).toString());
		}
	}
}
