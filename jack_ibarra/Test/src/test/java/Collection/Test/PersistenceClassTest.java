package Collection.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PersistenceClassTest {

	PersistenceClass controller = new PersistenceClass();
		
	@Mock
	private List<Animal> animalList;
	
	@Mock
	private Map<Integer,Animal> animalMapById; 
	
	@Mock
	private List<Dog> dogList;
	
	@Mock
	private Map<String,Dog> dogMap;
	
	@Mock
	private HashSet<Animal> animalSet;
	
	
	@Before
	public void init(){
		
	}
	
		
	@Test(expected = )
	private void AnimalNotFoundByIdExceptionTest(){
		
	}

	@Test
	private void NoDogNameExceptionTest(){
		
	}
	
	@Test
	private void NoMoreAnimalsExceptionTest(){
		
	}
	
	@Test
	private void getAnimalByIdTest(){
		
	}

	@Test
	private void getDogByDogName(){
		
	}
	
	@Test
	private void getOldestAnimalAndRemove(){
		
	}
	
	
}
