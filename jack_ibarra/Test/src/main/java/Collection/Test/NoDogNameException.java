package Collection.Test;

public class NoDogNameException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoDogNameException() {
		super("No dogName Exception");
	}
}
