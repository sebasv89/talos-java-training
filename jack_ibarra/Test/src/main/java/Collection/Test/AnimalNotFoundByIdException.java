package Collection.Test;

public class AnimalNotFoundByIdException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public AnimalNotFoundByIdException() {
		super("No IdAnimal Found");
	}
}
