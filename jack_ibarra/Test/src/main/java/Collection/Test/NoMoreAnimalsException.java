package Collection.Test;

public class NoMoreAnimalsException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoMoreAnimalsException() {
		super("No more animals");
	}
}
