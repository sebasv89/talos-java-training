package com.talosdigital.selenium;

import org.junit.Assert;
import org.junit.Test;

public class BuySomethingTest {

	@Test
	public void buyHappyPathTest() {
		BuySomething buySomething = new BuySomething();
		buySomething.chocolates();
		buySomething.addToCart(0); 
		buySomething.chocolates();
		buySomething.addToCart(1);
		buySomething.chocolates();
		buySomething.addToCart(0);
		buySomething.chocolates();
		buySomething.addToCart(6);
		Assert.assertEquals(Boolean.TRUE, buySomething.countDistinctItems() > 1);	
	}
	
	
}
