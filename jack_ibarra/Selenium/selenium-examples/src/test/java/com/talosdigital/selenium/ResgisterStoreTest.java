package com.talosdigital.selenium;

import org.junit.Assert;
import org.junit.Test;

public class ResgisterStoreTest {
	
	@Test
	public void resgisterHappyPathTest() {
		RegisterStore registerStore = new RegisterStore();
		registerStore.fillForm("123456", "Jack123");
		registerStore.submit();
		Assert.assertEquals("Almacen creado exitosamente", registerStore.getSuccessMessage());
		registerStore.close();
	}
	
	@Test
	public void resgisterEmptyNitTest() {
		RegisterStore registerStore = new RegisterStore();
		registerStore.fillForm("", "Jacky");
		registerStore.submit();
		Assert.assertEquals("El nit no puede estar vacio", registerStore.getErrorMessage());
		registerStore.close();
	}

	
	@Test
	public void resgisterEmptyNameTest() {
		RegisterStore registerStore = new RegisterStore();
		registerStore.fillForm("1234", "");
		registerStore.submit();
		Assert.assertEquals("El nombre no puede estar vacio", registerStore.getErrorMessage());
		registerStore.close();
	}

}
