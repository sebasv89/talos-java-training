package com.talosdigital.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class RegisterRetailOutlet {
	private final WebDriver webDriver;

	public RegisterRetailOutlet() {
		webDriver = new FirefoxDriver();
		webDriver.get("http://10.0.55.154:8080/safebuy/registrarPuntoDeVenta.html");
	}
	
	public void close() {
		webDriver.close();
	}
	
	public void submit() {
		WebElement button = webDriver.findElement(By.className("myButton"));
		button.submit();
	}

	public void fillForm(String name, String address, String store, String longitud,
			String latitud, String altitud) {
		webDriver.findElement(By.xpath("//input[@name= 'nombre']")).sendKeys(
				name);
		webDriver.findElement(By.xpath("//input[@name= 'longitude']")).sendKeys(
				longitud);
		webDriver.findElement(By.xpath("//input[@name= 'direccion']")).sendKeys(
				address);
		webDriver.findElement(By.xpath("//input[@name= 'latitude']")).sendKeys(
				latitud);
		
		webDriver.findElement(By.xpath("//select[@name= 'almacenNit']/option[@value='"+store+"']")).click();
		
		webDriver.findElement(By.xpath("//input[@name= 'altitude']")).sendKeys(
				altitud);
	}
	
	public String getErrorMessage() {
		return webDriver.findElement(By.className("error")).getText(); 
	}
	
	public String getSuccessMessage() {
		return webDriver.findElement(By.className("success")).getText();
	}

}
