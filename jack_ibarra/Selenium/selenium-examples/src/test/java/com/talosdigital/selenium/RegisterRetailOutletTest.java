package com.talosdigital.selenium;

import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

public class RegisterRetailOutletTest {

	@Test
	public void resgisterHappyPathTest() {
		RegisterRetailOutlet registerRetailOutlet = new RegisterRetailOutlet();
		registerRetailOutlet.fillForm("Punto 2"+ new Random().nextInt(),"Calle 10"+ new Random().nextInt(),"123",""+ new Random().nextInt(),""+ new Random().nextInt() ,""+ new Random().nextInt());
		registerRetailOutlet.submit();
		Assert.assertEquals("Punto de venta creado exitosamente", registerRetailOutlet .getSuccessMessage());
		registerRetailOutlet .close();
	}
	
	@Test
	public void resgisterEmptyAllTest() {
		RegisterRetailOutlet registerRetailOutlet = new RegisterRetailOutlet();
		registerRetailOutlet.fillForm("","","123","","","");
		registerRetailOutlet.submit();
		Assert.assertEquals("la direcci�n no puede ser vacia", registerRetailOutlet .getErrorMessage());
		registerRetailOutlet .close();
	}
	
	@Test
	public void resgisterEmptyNameTest() {
		RegisterRetailOutlet registerRetailOutlet = new RegisterRetailOutlet();
		registerRetailOutlet.fillForm("",""+ new Random().nextInt(),"123","","","");
		registerRetailOutlet.submit();
		Assert.assertEquals("El nombre no puede ser vacio", registerRetailOutlet .getErrorMessage());
		registerRetailOutlet .close();
	}
	
	
}
