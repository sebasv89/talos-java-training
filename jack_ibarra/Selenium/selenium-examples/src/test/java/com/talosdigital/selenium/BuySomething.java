package com.talosdigital.selenium;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BuySomething {

	private final WebDriver webDriver;

	public BuySomething() {
		webDriver = new FirefoxDriver();
		webDriver.get("http://juanregala.com/");
	}

	public String getTitle() {
		return webDriver.getTitle();
	}

	public void close() {
		webDriver.close();
	}
	
	public void chocolates() {
		List<WebElement> link = webDriver.findElements(By.id("category_3"));
		link.get(1).click();
	}
	public void submit() {
		WebElement button = webDriver.findElement(By.id("sendButton"));
		button.submit();
	}
	
	public void addToCart(int itemNumber) {
		List<WebElement> chocolates = webDriver.findElements(By.xpath("//*[@class='btn btn-block']"));
		chocolates.get(itemNumber).click();
	}
	
	public int countDistinctItems() {
		List<WebElement> items=webDriver.findElements(By.xpath("//table[@class='cart-table']//tbody//tr"));
		return items.size()-1;
	}

	public String getErrorMessage() {
		return webDriver.findElement(By.className("error")).getText();
	}

	public String getSuccessMessage() {
		return webDriver.findElement(By.className("success")).getText();
	}

	public void fillForm(String userName, String name, String lastName,
			String birthday, String email) {
		webDriver.findElement(By.xpath("//input[@name= 'usuario']")).sendKeys(
				userName);
		webDriver.findElement(By.xpath("//input[@name= 'nombre']")).sendKeys(
				name);
		webDriver.findElement(By.xpath("//input[@name= 'apellido']")).sendKeys(
				lastName);
		webDriver.findElement(By.xpath("//input[@name= 'fechaNacimiento']"))
				.sendKeys(birthday);
		webDriver.findElement(By.xpath("//input[@name = 'email']")).sendKeys(
				email);
	}

}

