package com.talosdigital.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class RegisterStore {
	private final WebDriver webDriver;

	public RegisterStore() {
		webDriver = new FirefoxDriver();
		webDriver.get("http://10.0.55.154:8080/safebuy/registrarAlmacen.html");
	}
	
	public void close() {
		webDriver.close();
	}
	
	public void submit() {
		WebElement button = webDriver.findElement(By.className("myButton"));
		button.submit();
	}

	public void fillForm(String nit, String name) {
		webDriver.findElement(By.xpath("//input[@name= 'nitAlmacen']")).sendKeys(
				nit);
		webDriver.findElement(By.xpath("//input[@name= 'nombreAlmacen']")).sendKeys(
				name);
		
	}
	
	public String getErrorMessage() {
		return webDriver.findElement(By.className("error")).getText(); //El nombre no puede estar vacio
	}																	//El nit no puede estar vacio
																		//Almacen creado exitosamente

	public String getSuccessMessage() {
		return webDriver.findElement(By.className("success")).getText();
	}

	
	
}
