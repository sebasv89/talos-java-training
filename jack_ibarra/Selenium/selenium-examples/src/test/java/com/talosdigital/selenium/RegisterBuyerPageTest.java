package com.talosdigital.selenium;

import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

public class RegisterBuyerPageTest {

	@Test
	public void testTitle() {
		RegisterBuyerPage page = new RegisterBuyerPage();
		String title = page.getTitle();
		Assert.assertEquals("Registrar Comprador", title);
		page.close();
	}

	@Test
	public void testEmpty() {
		RegisterBuyerPage page = new RegisterBuyerPage();
		page.submit();
		String message = page.getErrorMessage();
		Assert.assertEquals("El Nombre no puede estar vacio", message);
		page.close();
	}

	@Test
	public void testHappyPath() {
		RegisterBuyerPage page = new RegisterBuyerPage();
		page.fillForm("svelez" + new Random().nextInt(), "Sebastian", "Velez",
				"05-05-2013", "sebasv89@gmail.com");
		page.submit();
		String message = page.getSuccessMessage();
		Assert.assertEquals("Comprador creado", message);
		page.close();
	}

	@Test
	public void testDuplicatedUser() {
		RegisterBuyerPage page = new RegisterBuyerPage();
		String userName = "svelez" + new Random().nextInt();
		page.fillForm(userName, "Sebastian", "Velez", "05-05-2013",
				"sebasv89@gmail.com");
		page.submit();
		page.close();

		page = new RegisterBuyerPage();
		page.fillForm(userName, "Sebastian", "Velez", "05-05-2013",
				"sebasv89@gmail.com");
		page.submit();
		String message = page.getErrorMessage();
		Assert.assertEquals("El usaurio comprador ya existe: " + userName,
				message);
		page.close();
	}
}
