import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class MyFirstClass {

	@Test
	public void titleTest() {

		// Setup
		//System.setProperty("webdriver.chrome.driver", "/path/to/chromedriver");
		WebDriver webDriver = new FirefoxDriver();

		// Exercise
		webDriver.get("http://10.0.55.154:8080/safebuy/registrarAlmacen.html");

		// Verify
		String title = webDriver.getTitle();
		Assert.assertEquals("Dream Live Website Template | Contact :: W3layouts", title);

		// Tear down
		webDriver.close();

	}
	
	@Test
	public void resgisterEmptyTest() {

		// Setup
		//System.setProperty("webdriver.chrome.driver", "/path/to/chromedriver");
		WebDriver webDriver = new FirefoxDriver();

		// Exercise
		webDriver.get("http://10.0.55.154:8080/safebuy/registrarAlmacen.html");

		// Verify
		String title = webDriver.getTitle();
		Assert.assertEquals("Dream Live Website Template | Contact :: W3layouts", title);

		// Tear down
		webDriver.close();

	}
}
