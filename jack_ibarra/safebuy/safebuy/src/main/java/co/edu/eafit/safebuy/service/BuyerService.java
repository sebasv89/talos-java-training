package co.edu.eafit.safebuy.service;

import co.edu.eafit.safebuy.domain.model.comprador.Buyer;


public interface BuyerService {
	void save(Buyer buyer);
}
