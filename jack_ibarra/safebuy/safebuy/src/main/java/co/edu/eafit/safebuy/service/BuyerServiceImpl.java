package co.edu.eafit.safebuy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import co.edu.eafit.safebuy.domain.model.comprador.Buyer;
import co.edu.eafit.safebuy.infraestructure.persistence.util.PersistenceService;

@Component
public class BuyerServiceImpl implements BuyerService {

	@Autowired
	private PersistenceService persistenceService;
	
	@Override
	public void save(Buyer buyer) {
		persistenceService.save(buyer);
	}

}
