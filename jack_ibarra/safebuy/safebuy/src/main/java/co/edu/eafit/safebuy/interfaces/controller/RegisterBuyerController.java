package co.edu.eafit.safebuy.interfaces.controller;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import co.edu.eafit.safebuy.domain.model.comprador.Buyer;
import co.edu.eafit.safebuy.service.BuyerService;
import co.edu.eafit.safebuy.util.MessageType;
import co.edu.eafit.safebuy.util.ResponseMessage;

@Controller
public class RegisterBuyerController {

	
	public BuyerService buyerService;
	
	@RequestMapping("/registerBuyer.html")
	public ModelAndView getPage() {
		return new ModelAndView("/pages/registerBuyer.jsp");
	}

	@RequestMapping("/registerBuyerAction.html")
	public ModelAndView submitForm(
			@RequestParam String user,
			@RequestParam String name,
			@RequestParam String lastName,
			@RequestParam String birthDay,
			@RequestParam String email,
			@RequestParam(required = false, defaultValue = "false") Boolean receiveNotification)
			throws ParseException {
		
		Buyer buyer = new Buyer();
		buyer.setLastName(lastName);
		buyer.setName(name);
		buyerService.save(buyer);
		return ResponseMessage.createMessage("Buyer created.",
				MessageType.success);
	}
}
