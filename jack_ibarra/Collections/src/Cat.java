public class Cat extends Mammer {

	@Override
	public void doAMammerThing() {
		System.out.println("Executing Do a mammer thing!!");
		
	}

	@Override
	public void live() {
		System.out.println("Executing Live!");
		
	}
	
	public void anotherDogMethod(){
		System.out.println("Executing another cat method!");
	}
	
	public int add (Integer a, Integer b){
		return a + b;
	}

}
