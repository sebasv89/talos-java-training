

public class Dog extends Mammer {
	
	private String dogName;
	
	@Override
	public void doAMammerThing() {
		System.out.println("Executing Do a mammer thing!!");
		
	}

	@Override
	public void live() {
		System.out.println("Executing dog Live!");
		
	}
	
	public void anotherDogMethod(){
		System.out.println("Executing another dog method!");
	}
	
	public int add (Integer a, Integer b){
		return a + b;
	}

	public String getDogName() {
		return dogName;
	}

	public void setDogName(String dogName) {
		this.dogName = dogName;
	}

}
