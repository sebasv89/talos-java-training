import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class PersistenceClass {
	private List<Animal> animalList = new ArrayList<Animal>(); 
	private Map<Integer,Animal> animalMapById = new HashMap<Integer, Animal>(); 
	private List<Dog> dogList = new ArrayList<Dog>();
	private Map<String,Dog> dogMap = new HashMap<String,Dog>();
	private HashSet<Animal> animalSet  = new HashSet<Animal>();
	
	public void add(Animal animal) {
		animalList.add(animal);
		animalMapById.put(animal.getId(),animal);
		animalSet.add(animal);
		if(animal instanceof Dog){
			dogMap.put(((Dog) animal).getDogName(),(Dog) animal);
			dogList.add((Dog) animal);
		}
	}
	
	public List<Animal> getAllAnimals(){
		return animalList;
	}
	
	public HashSet<Animal>	getUniqueAnimals() {
		return animalSet;
	}
	
	public List<Dog> getAllDogs() {
		return dogList;
	} 
	
	public Animal getAnimalById(int id) throws Exception {
		try {
			return animalMapById.get(id);
		} catch (Exception e) {
			throw new Exception("No idAnimal found");
		}
	} 
	
	public Dog getDogByDogName(String dogName) throws Exception {
		try {
			return dogMap.get(dogName);
		} catch (Exception e) {
			throw new Exception("No dogName found");
		}
		
	} 
	public Animal getOldestAnimalAndRemove() throws Exception {
		try {
			return animalList.remove(0);
		} catch (Exception e) {
			throw new Exception("No more animals");
		}
	}
	
}
