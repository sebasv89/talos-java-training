

public class GenericsExampleToAvoidExplicitCasting {

	
	public <T extends Animal> T modifyMyAnimals(T object){
		
		if (object instanceof Mammer){
			((Mammer)object).doAMammerThing();
		}
		return object;
	}
}
