import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;


public class Collection {
	
	static List<Animals> animalList = new ArrayList<>();
	static Set<Animals> animalSet = new HashSet<>();
	static Map<String,List<Animals>> animalMap = new HashMap<>();
	static Map<String,Animals> animalMap2 = new HashMap<>();
	static Queue<Animals> animalQueue= new LinkedList<>();
	
	
	public static void main (String args[]){
		
		//getAllAnimals (List, Set, Queue)
		
		Cat firstCat = new Cat();
		firstCat.setName("First Cat");
		add(firstCat);
		add(firstCat);
		Cat secondCat = new Cat();
		secondCat.setName("Second Cat");
		add(secondCat);
		
		Dog firstDog = new Dog();
		firstDog.setName("First Dog");
		add(firstDog);
		Dog secondDog = new Dog();
		secondDog.setName("Second Dog");
		add(secondDog);
		
		//getAllDogs (Map<"Dog",List<Dog> >)
		
		List<Animals> myDogList = new ArrayList<>();
		
		Dog dog1 = new Dog();
		dog1.setName("Doggy 1");
		myDogList.add(dog1);
		Dog dog2 = new Dog();
		dog2.setName("Doggy 2");
		myDogList.add(dog2);
		Dog dog3 = new Dog();
		dog3.setName("Doggy 3");
		myDogList.add(dog3);
		
		addMap("Dog",myDogList);
		
		//getAnimalById
		
		Cat cat1 = new Cat();
		cat1.setId(1);
		cat1.setName("Kitty 1");
		Cat cat2 = new Cat();
		cat2.setId(2);
		cat2.setName("Kitty 2");
		Cat cat3 = new Cat();
		cat3.setId(3);
		cat3.setName("Kitty 3");
		
		addMap2(cat1, String.valueOf(cat1.getId()));
		addMap2(cat2, String.valueOf(cat2.getId()));
		addMap2(cat3, String.valueOf(cat3.getId()));
		
		Dog dog4 = new Dog();
		dog4.setDogName("little Dog");
		dog4.setName("Dog4 second Name");
		Dog dog5 = new Dog();
		dog5.setDogName("second little Dog");
		dog5.setName("Dog5 second Name");
		
		addMap2(dog4, dog4.getDogName());
		addMap2(dog5, dog5.getDogName());
		
		for (Animals animals : animalList) {
			
			System.out.println(animals.getName() + " List");
			
		}
		
		System.out.println("-----------------");
		
		for (Animals animals : animalSet) {
			
			System.out.println(animals.getName() + " Set");
			
		}
		
		System.out.println("-----------------");
		
		for (Animals animals : animalQueue) {
			
			System.out.println(animals.getName() + " Queue");
		}
		
		System.out.println("-----------------");
		
		for(Map.Entry<String, List<Animals>> entry : animalMap.entrySet()){
			
			System.out.println(" Key: " + entry.getKey());
			
			for (Animals animals : entry.getValue()) {
				
				System.out.println("Value: "+ animals.getName());
			}
			
		}
		
		System.out.println("-----------------");
		
		String key = "1";
		System.out.println(animalMap2.get(key).getName());
		
		System.out.println("-----------------");
		
		String key2 = "little Dog";
		System.out.println(animalMap2.get(key2).getName());
		

	}
	
	public static void add(Animals a){
		
			animalList.add(a);
			animalSet.add(a);
			animalQueue.add(a);
		
	}
	
	public static void addMap2(Animals a, String i){
		
		animalMap2.put(i, a);
	
}

	public static void addMap(String key, List<Animals> myDogList){
		
		animalMap.put(key,myDogList);
	
}
	
	public static List<Animals> getAllAnimals(){
		
		
		return animalList;
		
			
	}
	
	public static Set<Animals> getUniqueAnimals(){
		
		return animalSet;
		
	}
	
	public static Map<String,List<Animals>> getAllDogs(){
		return animalMap;
		
	}
	
	public static Map<String,Animals> getAnimalById(){
		return animalMap2;
		
	}
	
	public static Map<String,Animals> getDogByDogName(){
		return animalMap2;
		
	}
	
	public static Queue<Animals> getOldestAnimalAndRemove(){
		return animalQueue;
		
	}

}
