import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Test {

	public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException {

		Person a = new Person();
		a.setFirstName("David");
		a.setLastName("Sttivend");
		a.setAge(22);
		
		Field[] aFields = Person.class.getDeclaredFields();
		
		for (int i = 0; i < aFields.length; i++) {
			aFields[i].setAccessible(true);
			System.out.println("aFields["+i+"]"+aFields[i].get(a));
		}
	}
}
