
public class Person {
	
	private String firstName;
	private String lastName;
	private Dog dog;
	
	public Person(String firstName, String lastName, Dog dog) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.dog = dog;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Dog getDog() {
		return dog;
	}
	public void setDog(Dog dog) {
		this.dog = dog;
	}

}
