
public class Dog {
	
	private String dogName;
	private String dogBreed;
	private int dogAge;

	
	
	public Dog(String dogName, String dogBreed, int dogAge) {
		this.dogName = dogName;
		this.dogBreed = dogBreed;
		this.dogAge = dogAge;
	}
	public String getDogName() {
		return dogName;
	}
	public void setDogName(String dogName) {
		this.dogName = dogName;
	}
	public String getDogBreed() {
		return dogBreed;
	}
	public void setDogBreed(String dogBreed) {
		this.dogBreed = dogBreed;
	}
	public int getDogAge() {
		return dogAge;
	}
	public void setDogAge(int dogAge) {
		this.dogAge = dogAge;
	}
}
