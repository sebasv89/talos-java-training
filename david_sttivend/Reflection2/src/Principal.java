import java.lang.reflect.Field;
import java.util.Iterator;

public class Principal {

	/**
	 * @param args
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 */
	public static void main(String[] args) throws IllegalArgumentException,
			IllegalAccessException {

		Dog dog = new Dog("Rocky", "Labrador", 7);

		Person persona = new Person("David", "Sttivend", dog);

		metodo(persona, 0);
	}

	public static void metodo(Object objeto, int identation)
			throws IllegalArgumentException, IllegalAccessException {

		Field[] fields = objeto.getClass().getDeclaredFields();

		for (Field field : fields) {

			field.setAccessible(true);
			Class typeObject = field.getType();

			if (typeObject.isPrimitive() || typeObject.equals(String.class)) {

				
				for (int i = 1; i <= identation; i++) {

					System.out.print("  ");
				}
				
				System.out.print(field.get(objeto));
				System.out.println();
			} else {
				
				metodo(field.get(objeto), identation+1);
			}
		}

	}

}
